<?php
/*
	Template Name: News Overview
*/
?>

<?php get_header(); ?>

	<div class="section_introduction vector-control-introduction wrap">
		<div class="left-col">
			<h1><?php the_title(); ?></h1>

			<div class="content">
				<?php the_field('introductory_content', $post->ID); ?>
			</div>
			<div class="author-info">
				<?php $avatar = get_field('contact_picture', $post->ID ); ?>
				<img class="author-pic" src="<?php echo $avatar; ?>" alt="Image of <?php the_field('contact_name', $post->ID); ?>">
				<div class="author-details">
					<?php the_field('contact_name', $post->ID); ?><br>
					<a href="mailto:<?php the_field('contact_email_address', $post->ID); ?>" aria-label="Email <?php the_field('contact_name', $post->ID); ?> - opens up your email client."><?php the_field('contact_email_address', $post->ID); ?></a>
				</div>
			</div>
		</div>
	</div>

	<div class="grid-container">
		<div class="wrap">
			<div class="filters-container desktop-filters">
				<p>FILTER:</p>
				<div class="single-filter-group-container">
					<div class="filter all-filter active-filter">All</div>
				</div>

				<?php $categories = get_terms('category');
				$categoryHierarchy = array();
				sort_terms_hierarchicaly($categories, $categoryHierarchy); ?>

				<?php foreach($categoryHierarchy as $category): ?>
					<div class="single-filter-group-container">
						<div class="filter" data-related=".<?php echo $category->slug; ?>"><?php echo $category->name; ?></div>

						<?php if($category->children): ?>
							<div class="sub-filters">
								<?php foreach($category->children as $category): ?>
									<div class="filter sub-filter" data-related=".<?php echo $category->slug; ?>"><?php echo $category->name; ?></div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>

			<div class="filters-container mobile-filter">
				<div class="mobile-filter-trigger">
					<p>Filter By:</p>
					<img src="<?php image('red-chevron.svg') ?>">
				</div>

				<?php $categories = get_terms('category');
				$categoryHierarchy = array();
				sort_terms_hierarchicaly($categories, $categoryHierarchy); ?>

				<ul>
					<?php foreach($categoryHierarchy as $category): ?>
						<?php if(!$category->parent): ?>
							<li class="filter" data-related=".<?php echo $category->slug; ?>"><?php echo $category->name; ?></li>
						<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>

		<div class="grid wrap">
			<div class="grid-sizer"></div>
			<div class="gutter-sizer"></div>
			<?php $posttype = 'post'; ?>
			<? $posts = 0; ?>
			<?php get_overview_grid($posttype, $posts) ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
