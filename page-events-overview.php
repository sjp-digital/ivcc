<?php
/*
	Template Name: Events Overview
*/
?>

<?php get_header(); ?>

	<div class="section_introduction wrap">
		<div class="left-col">
			<h1><?php the_title(); ?></h1>

			<div class="content">
				<?php the_field('introductory_content', $post->ID); ?>
			</div>
		</div>
	</div>

	<div class="grid-container">
		<div class="wrap">
			<div class="filters-container desktop-filters">
				<div class="single-filter-group-container">
					<div class="filter active-filter" data-related=".upcoming">Upcoming</div>
				</div>
				<div class="single-filter-group-container">
					<div class="filter archive" data-related=".archive">Archive</div>
				</div>
			</div>

			<div class="filters-container mobile-filter">
				<div class="mobile-filter-trigger">
					<p>Filter By:</p>
					<img src="<?php image('red-chevron.svg') ?>">
				</div>

				<ul>
					<li class="filter active-filter" data-related=".upcoming">Upcoming</li>
					<li class="filter archive" data-related=".archive">Archive</li>
				</ul>
			</div>
		</div>

		<div class="event-map wrap">
			<div class="date-picker">
				<div class="year-selector selector">
					<div class="disabled arrow prev"></div>
					<p><?php echo date('Y') ?></p>
					<div class="arrow next"></div>
				</div>
				<div class="month-selector selector">
					<div class="disabled arrow prev"></div>
					<p><?php echo date('F') ?></p>
					<div class="arrow next"></div>
				</div>
				<div class="calendar-container owl-carousel">
				    <?php foreach ($months as $num => $name) {
						$str_explode=explode(",",$name);
						$currentMonth = $str_explode[0];
						$currentYear = $str_explode[1]; ?>

		   				<?php $dateComponents = getdate();

					     $month = $dateComponents['mon'];
					     $year = $dateComponents['year']; ?>

						<div class="month" visibility-date-related="<?php echo $currentYear.'-'.date('m', mktime(0, 0, 0, $currentMonth,  1)); ?>" month-related="<?php echo date('F', mktime(0, 0, 0, $currentMonth,  1)); ?>" year-related="<?php echo $currentYear; ?>">

					    	<?php echo build_calendar($currentMonth,$currentYear); ?>

					    </div>
					<? } ?>
				</div>
			</div>

			<div class="map-container">
				<div class="map-overlay">
					<?php event_points() ?>
				</div>
				<img src="<?php image('events-map.svg') ?>">
			</div>
		</div>

		<div class="grid wrap">
			<div class="grid-sizer"></div>
			<div class="gutter-sizer"></div>
			<?php $posttype = 'events'; ?>
			<? $posts = 0; ?>

			<?php get_overview_grid($posttype, $posts) ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
