jQuery(document).ready(function($) {

	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}

	//========== GET TEAM MEMBER CONTENT  ==========//
		var personID = getParameterByName('person');
		var postID;
		var remodalOldElement;

		if(personID) {
			$('.remodal').addClass('team-modal');
			get_team_content(personID);

			var inst = $('[data-remodal-id=ajax-modal]').remodal();
			inst.open();
		}

		$('.search-result .personnel, .team-modal-link').click(function() {
			postID = $(this).attr('postid');
			$('.remodal').addClass('team-modal');
			$remodalOldElement = $(this);

			get_team_content(postID);
			$('.remodal').focus();
		});

		$('.remodal-close').click(function() {
			$remodalOldElement.focus();
		});

		function get_team_content(postID) {
		    $.ajax({
		        url: ajax_script.ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
		 		data: {
		            'action': 'get_team_content',
		           	'postid': postID,
		        },
		        beforeSend: function () {
		        	$('.modal-content').removeClass('active');
		        	$('.modal-loader').addClass('show-loader');
				},
		        success:function(data) {
		        	setTimeout(function() {
		        		$('.modal-loader').removeClass('show-loader');
		        		$('.modal-content').addClass('active');
		        	}, 1500);
		        	$('.modal-content').html(data);
		        },
		        error: function(errorThrown){
		            console.log(errorThrown);
		            console.log('error');
		        }
		    });
		}

		$('.remodal-close').click(function() {
			$('.modal-content').empty();
			$('.remodal').removeClass('team-modal');
		});
});
