jQuery(document).ready(function($) {

	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}


	//========== GET RESOURCES CONTENT  ==========//
		var resourceID = getParameterByName('resource');
		var postID;

		if(resourceID) {
			$('.remodal').addClass('resource-modal');
			get_resource_content(resourceID);

			var inst = $('[data-remodal-id=ajax-modal]').remodal();
			inst.open();
		}

		$('.resources-grid-col, .search-result .resources').click(function() {
			postID = $(this).attr('postid');
			$('.remodal').addClass('resource-modal');
			get_resource_content(postID);
		});

		$('.resources-grid-col > a').click(function() {
			$remodalOldElement = $(this);
		});

		function get_resource_content(postID) {
		    $.ajax({
		        url: ajax_script.ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
		 		data: {
		            'action': 'get_resource_content',
		           	'postid': postID,
		        },
		        beforeSend: function () {
		        	$('.modal-content').removeClass('active');
		        	$('.modal-loader').addClass('show-loader');
				},
		        success:function(data) {
		        	setTimeout(function() {
		        		$('.modal-loader').removeClass('show-loader');
		        		$('.modal-content').addClass('active');
		        	}, 1500);
		        	$('.ajax-modal .modal-content').html(data);
		        },
		        error: function(errorThrown){
		            console.log(errorThrown);
		            console.log('error');
		        }
		    });
		}

		$('.research-paper .view').click(function() {
			postID = $(this).attr('postid');
			$('.remodal').addClass('resource-modal');

			get_download_content(postID);
		});

		var downloadID = getParameterByName('download');

		if(downloadID) {
			$('.remodal').addClass('resource-modal');
			get_download_content(downloadID);

			var inst = $('[data-remodal-id=ajax-modal]').remodal();
			inst.open();
		}

		function get_download_content(postID) {
		    $.ajax({
		        url: ajax_script.ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
		 		data: {
		            'action': 'get_download_content',
		           	'postid': postID,
		        },
		        beforeSend: function () {
		        	$('.modal-content').removeClass('active');
		        	$('.modal-loader').addClass('show-loader');
				},
		        success:function(data) {
		        	setTimeout(function() {
		        		$('.modal-loader').removeClass('show-loader');
		        		$('.modal-content').addClass('active');
		        	}, 1500);
		        	$('.ajax-modal .modal-content').html(data);
		        },
		        error: function(errorThrown){
		            console.log(errorThrown);
		            console.log('error');
		        }
		    });
		}


		$('.remodal-close').click(function() {
			$('.modal-content').empty();
			$('.remodal').removeClass('resource-modal');
		});

		// Footer Documents
		$('#menu-footer-menu .menu-item.menu-item--download a').each(function() {
			$(this).attr('data-remodal-target', 'ajax-modal');
		});

		$('#menu-footer-menu .menu-item.menu-item--download a').click(function(e) {
			e.preventDefault();
			postID = $(this).parent('.menu-item').attr('class').match(/\d+/)[0];
			console.log('Download ID: ' + postID);
			$('.remodal').addClass('resource-modal');
			get_download_content(postID);
		});
});
