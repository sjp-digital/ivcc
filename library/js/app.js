jQuery(document).ready(function($) {

//========== LOADER ==========//
    $('.page-loader').addClass('page-loader--hidden');
    $('.page-loader').removeClass('page-loader--change');

    function page_change() {
     var windowWidth = $(window).width()

        if(windowWidth > 992) {
         $('a').not('a[href*="#"], .fancybox-link, a[href^="mailto:"], a[href^="tel:"], .remodal-close, .social-share__link, a[target="_blank"], a[data-remodal-target="resource-modal"], .download-link, .newsletter-link, .research-download').click(function(e) {
           e.preventDefault();
           $('.page-loader').addClass('page-loader--change');
           setTimeout(function(url) { window.location = url }, 250, this.href);
         });
        }
    }

    page_change();

	window.addEventListener("pageshow", function(evt){
	        if(evt.persisted){
	        setTimeout(function(){
	            window.location.reload();
	        },10);
	    }
	}, false);

//========== HP NAV  ==========//
var windowWidth = $(window).width()
if(windowWidth >= 1024) {
	var waypoints = $('.hp-masthead').waypoint(function(direction) {
	   $('.fixed-header').toggleClass('active');
	}, {
	  offset: '-600px'
	})
}

//========== SMALLER NAV ON SCROLL  ==========//
var windowWidth = $(window).width()
if(windowWidth >= 1024) {
	$(window).scroll(function() {
	    var scroll = $(window).scrollTop();
	    if (scroll >= 150) {
	    	$(".page-nav").addClass("condensed-header");
	        $("header").not('.front-page-header').addClass("condensed");
	    } else {
	    	$("header").not('.front-page-header').removeClass("condensed");
	    	$(".page-nav").removeClass("condensed-header");
	    }
	});
}

//========== MOBILE NAV  ==========//
	$('.nav-icon').click(function(){
		$(this).toggleClass('open');
		$('header').toggleClass('active-nav');
		$('nav').toggleClass('active-nav');
		$('body').toggleClass('noscroll');
		$('html').toggleClass('noscroll');
	});

//========== MOBILE PAGE NAV  ==========//
	$('.page-nav-trigger').click(function(){
		$(this).toggleClass('open');
		$('.page-nav').toggleClass('active-nav');
	});

	$('.page-nav a').click(function(){
		$('.page-nav').scrollTop('.page-nav-trigger');
		$('.page-nav-trigger').removeClass('open');
		$('.page-nav').removeClass('active-nav');
	});

//========== MOBILE FILTERS  ==========//
	$('.mobile-filter-trigger').click(function(){
		$(this).toggleClass('open');
		$('.mobile-filter').toggleClass('active-filter');
	});

	$('.mobile-filter li').click(function(){
		$('.mobile-filter-trigger').removeClass('open');
		$('.mobile-filter').removeClass('active-filter');
	});

//========== HP CONTENT FILTER ==========//
	$('.hp-filters .filter').click(function(){
		console.log('true');
		$('.right-col-content .intro-content').removeClass('active-content');
		$('.maps .map').removeClass('active-map active-dots');
		$('.hp-filters .filter').removeClass('active-filter');
		$(this).addClass('active-filter');

		var relatedFilter = $(this).attr('data-related');
		$('.maps .map[data-related='+relatedFilter+']').addClass('active-map  active-dots');
		$('.right-col-content .intro-content[data-related='+relatedFilter+']').addClass('active-content');
	});

	//========== HP MAP POINT ACTIVATION  ==========//
		$('.map .location').hover(function() {
			$('.map .location').removeClass('active-location');
			$(this).addClass('active-location');
		});

		$('.map .location').focus(function() {
			$('.map .location').removeClass('active-location');
			$(this).addClass('active-location');
		});

		$(document).click(function(e) {
		if ( $(e.target).closest('.map .location-marker').length === 0 ) {
		  	$('.map .location').removeClass('active-location');
		  }
		});

		if(windowWidth >= 1024) {
			var waypoints = $('.maps').waypoint(function(direction) {
				this.destroy()
			   $('.active-map').toggleClass('active-dots');
			}, {
			  offset: '90%'
			})
		}
//========== RESEARCH PAPER CAROUSEL  ==========//
	$('.papers-carousel').owlCarousel({
	    loop:false,
	    nav: false,
	    items: 1,
	    dots: true,
	    navText: ['',''],
	    autoHeight: false,
	    mouseDrag: true,
	    touchDrag: false,
	   	margin: 0,
	})

//========== GRID  ==========//
	$grid = $('.grid');
	$('.grid').isotope({
		itemSelector: '.grid-col',
		percentPosition: true,
		layoutMode: 'fitRows',
	  	fitRows: {
		  	columnWidth: '.grid-sizer',
		  	gutter: '.gutter-sizer',
	  	}
	});

	var parts = window.location.href.split('#');
	if (parts.length > 1) {
		parts = parts[1];

		if (parts.length > 1) {
			$grid.isotope({ filter: '.'+parts });

			$('.filter').removeClass('active-filter');

			$('.filter[data-related=".'+parts+'"]').addClass('active-filter');
			var selectedFilter = $('.'+parts+'filter');

			if($(selectedFilter).hasClass('sub-filter')) {
				$(selectedFilter).closest('.single-filter-group-container').addClass('active-filter-group');
			}
		}
	}

	$('.all-filter').click(function(){
		$grid.isotope({ filter: '*' });
		$( ".filter").removeClass('active-filter');
		$('.all-filter').addClass('active-filter');
	});

	$('.filters-container .filter').click(function(){
		$( ".filter").removeClass('active-filter');
		$(this).addClass('active-filter');

		$('.grid-col').addClass('fadeIn');

		if($(this).hasClass('sub-filter')) {
			$(this).addClass('active-filter');
		} else {
			$( ".single-filter-group-container").removeClass('active-filter-group');
			$(this).parent('.single-filter-group-container').addClass('active-filter-group');
		}

		var filterValue = $(this).attr('data-related');
		$grid.isotope({ filter: filterValue });
	});

	if($('body').hasClass('page-template-page-events-overview')) {
		$grid.isotope({ filter: '.upcoming' });
	}

	$('.filter').contents().filter(function() {
 		return this.nodeType == 3; // Text node
	}).each(function() {
	    this.data = this.data.replace(/ /g, '\u00a0');
	});

//========== EVENTS PAGE  ==========//
    if(windowWidth >= 1024) {
		if($('body').hasClass('page-template-page-events-overview')) {
			$('.filters-container .filter').click(function(){
				if($(this).hasClass('archive')) {
					$('.event-map').hide();
				} else {
					$('.event-map').show();
				}
			});
		}

		var owl = $('.calendar-container');
		owl.owlCarousel({
		    loop: false,
		    nav: false,
		    items: 1,
		    dots: false,
		    navText: ['',''],
		    autoHeight: true,
		    mouseDrag: false,
		    touchDrag: false,
		   	margin: 0,
		   	onInitialized: counter
		})

		 var items;
		 var currentItem;

		function counter(event) {
		   	items = event.item.count;
		   	currentItem = event.item.index;
		}

		var currentmonthVisibility= $('.owl-item.active .month').attr('visibility-date-related');

		$('.map-container .point').hide();
	   	$('.map-container .point[visibility-start-date='+currentmonthVisibility+']').show();
	   	$('.map-container .point[visibility-end-date='+currentmonthVisibility+']').show();

		$('.month-selector .arrow.next').click(function() {
		    owl.trigger('next.owl.carousel');
		})

		$('.month-selector .arrow.prev').click(function() {
		    owl.trigger('prev.owl.carousel');
		})

		$('.year-selector .arrow.next').click(function() {
			var yearForward = currentItem + 12;
		  	owl.trigger('to.owl.carousel', yearForward);
		})

		$('.year-selector .arrow.prev').click(function() {
			var yearBack = currentItem - 12;
		    owl.trigger('to.owl.carousel', yearBack);
		})

		owl.on('changed.owl.carousel', function(event) {
	        currentItem = event.item.index;
	        currentMonth = $('.owl-item.active .month').attr('month-related');

	        if(currentItem == '0') {
	        	$('.month-selector .prev').addClass('disabled');
	        	$('.year-selector .prev').addClass('disabled');
	        } else if(currentItem == (items - 1)) {
	        	$('.month-selector .next').addClass('disabled');
	        	$('.year-selector .next').addClass('disabled');
	        }
	        else {
	        	$('.month-selector .next').removeClass('disabled');
	        	$('.month-selector .prev').removeClass('disabled');
	        	$('.year-selector .next').removeClass('disabled');
	        	$('.year-selector .prev').removeClass('disabled');
	        }
	    })

		$('.selector .arrow').click(function() {
	        var currentMonth = $('.owl-item.active .month').attr('month-related');
	        $('.month-selector p').html(currentMonth);

	        var currentYear = $('.owl-item.active .month').attr('year-related');
	        $('.year-selector p').html(currentYear);

	        var currentmonthVisibility= $('.owl-item.active .month').attr('visibility-date-related');

	        $('.map-container .point').hide();
	       	$('.map-container .point[visibility-start-date='+currentmonthVisibility+']').show();
	   		$('.map-container .point[visibility-end-date='+currentmonthVisibility+']').show();
		});

		//== GET DATES BETWEEN START AND END DATES FUNCTION ==//
		var getDates = function(startDate, endDate) {
			var dates = [],
			currentDate = startDate,
			addDays = function(days) {
				var date = new Date(this.valueOf());
				date.setDate(date.getDate() + days);
				return date;
			};
			while (currentDate <= endDate) {
				dates.push(currentDate);
				currentDate = addDays.call(currentDate, 1);
			}
			return dates;
		};

		//== FORMAT DATE FUNCTION ==//
		function formatDate(date) {
			var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('-');
		}

		$('.map-container .point').each(function() {
			// Get start and end date
			var startDate = $(this).attr('date-related');
			var endDate = $(this).attr('end-date-related');

			// Format to work with function
			formattedstartDate = startDate.replace(/-/g, ',');
			formattedendDate = endDate.replace(/-/g, ',');

			//Run function with start and end dates
			var dates = getDates(new Date(formattedstartDate), new Date(formattedendDate));
			dates.forEach(function(date) {
				date = formatDate(date);

				//Populate date picker points
				$('.date-picker .day[date-related='+date+']').addClass('event-present');
				$('.date-picker .day[date-related='+date+']').attr('date-related', startDate);
			});
		});

		$('.map-container .point').hover(function() {
			var pointClicked = $(this).attr('date-related');
			var pointLocation = $(this).attr('location');

			$('.map-container .point').removeClass('second-event');
			$('.map-container .point').removeClass('activedate');
			$(this).addClass('activedate');

			$('.date-picker .day').removeClass('activedate');
			$('.date-picker .day[date-related='+pointClicked+']').addClass('activedate');

			var noofPoints = $('.map-container .point[location='+pointLocation+']').length;

			if(noofPoints > 1) {
				$('.map-container .point[location='+pointLocation+']').addClass('activedate');
				$(this).closest('.activedate').addClass('second-event');

				$('.activedate').each(function() {
					var secondpointClicked = $(this).attr('date-related');
					$('.date-picker .day[date-related='+secondpointClicked+']').addClass('activedate');
				});
			}
		});

		$('.date-picker .day').hover(function() {
			var pointClicked = $(this).attr('date-related');

			$('.map-container .point').removeClass('activedate');
			$('.date-picker .day').removeClass('activedate');
			$(this).addClass('activedate');
			$('.date-picker .day[date-related="'+pointClicked+'"]').addClass('activedate');
			$('.map-container .point[date-related='+pointClicked+']').addClass('activedate');
		});

		$(document).click(function(e) {
		if ( $(e.target).closest('.map-container .point, .date-picker .day, .point-details').length === 0 ) {
		 	$('.map-container .point, .date-picker .day').removeClass('activedate second-event');
		 }
		});
	}

//========== NEWSLETTER POPUP =========//
    if(localStorage.getItem('popState') != 'shown'){
    	setTimeout( function() {
    		$(".newsletter-popup-container").addClass('active-popup');
    	}, 2000);

        localStorage.setItem('popState','shown')
    }

    $('.newsletter-popup-container .close').click(function(e) {
        $(".newsletter-popup-container").removeClass('active-popup');
    });

    if($('body').hasClass('home')) {
		var container = $('.newsletter-popup');

		$(document).click(function(e) {
			if (!container.is(e.target) && container.has(e.target).length === 0)  {
				$(".newsletter-popup-container").removeClass('active-popup');
			}
		});
	}

//========== MOBILE NAV  ==========//
	$('.search-filters .filter').click(function(){
		//window.location = "&filter='project'";
		function updateQueryStringParameter(uri, key, value) {
		  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
		  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		  if (uri.match(re)) {
		    return uri.replace(re, '$1' + key + "=" + value + '$2');
		  }
		  else {
		    return uri + separator + key + "=" + value;
		  }
		}
	});

//========== INTERNAL PAGE NAV ==========//
	var windowWidth = $(window).width()
	// Select all links with hashes
	$('a[href*="#"]')
	  // Remove links that don't actually link to anything
	  .not('[href="#"]')
	  .not('[href="#0"]')
	  .click(function(event) {
	    // On-page links
	    if (
	      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
	      &&
	      location.hostname == this.hostname
	    ) {
	      // Figure out element to scroll to
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
	      // Does a scroll target exist?
	      if (target.length) {
	        // Only prevent default if animation is actually gonna happen
	        event.preventDefault();
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000, function() {
	          // Callback after animation
	          // Must change focus!
	          var $target = $(target);
	          $target.focus();
	          if ($target.is(":focus")) { // Checking if the target was focused
	            return false;
	          } else {
	            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
	            $target.focus(); // Set focus again
	          };
	        });
	      }
	    }
	  });

//========== FORM VALIDATION  ==========//
	document.addEventListener( 'wpcf7invalid', function( event ) {
		$('.wpcf7-form-control-wrap').removeClass('valid-field');
		$('.wpcf7-form-control-wrap').removeClass('invalid-field');
		$('.wpcf7-not-valid').parent('span').addClass('invalid-field');
		$('.wpcf7-form-control-wrap:not(.invalid-field)').addClass('valid-field');
	});

	var rootURL = window.location.protocol + "//" + window.location.host + "/"
	document.addEventListener( 'wpcf7mailsent', function( event ) {

	if(event.detail.contactFormId == '845') {
	    	window.location = 'https://ivcc.com/thank-you-for-signing-up'
	    } else {
	    	window.location = 'https://ivcc.com/thank-you'
	    }
	}, false );

	//     if(event.detail.contactFormId == '845') {
	//     	window.location = rootURL + '/thank-you-for-signing-up'
	//     } else {
	//     	window.location = rootURL + '/thank-you'
	//     }
	// }, false );

//========== READING TIME  ==========//
	$(function() {

		$('.read-time-count').each(function() {

			const _this = $(this);

			_this.readingTime({
				readingTimeTarget: _this.find('.eta'),
				success: function(data) {
				},
				error: function(data) {
					_this.find('.eta').remove();
				}
			});
		});
	});

//========== SET VIDEO HEIGHT  ==========//
	if(windowWidth > 768) {
		var videoheight = $('video').height();
		$('.hp-masthead').css('height', videoheight);
	}

//========== GRID FILTERS UPDATED ==========//
	$('.changeable-intro-content .content[grid-related=all]').show();

	$('.updated-filters .filter[grid-related=all]').addClass('active-filter');
	$('.grid-category-container[grid-related=all]').show();
	$grid.isotope('layout');

	$('.updated-filters .filter').click(function(){
		$('.changeable-intro-content .content').hide();
		$('.grid-category-container').hide();
		var relatedFilter = $(this).attr('grid-related');
		$('.grid-category-container[grid-related='+relatedFilter+']').fadeIn();
		$('.changeable-intro-content .content[grid-related='+relatedFilter+']').fadeIn();
		$grid = $('.grid');
		$grid.isotope('layout');
	});

//========== MARKET ACCESS FILTERS ==========//
	$('.market-access-filters .filter').click(function(){
		var relatedFilter = $(this).attr('grid-related');

        if(relatedFilter == 'ngenirs') {
        	$('.map').removeClass('new-nets-project');
	        $('.map').addClass('ngenirs');
	    } else if(relatedFilter == 'new-nets-project') {
	    	$('.map').removeClass('ngenirs');
	    	$('.map').addClass('new-nets-project');
	    } else {
	    	$('.map').removeClass('ngenirs');
	    	$('.map').removeClass('new-nets-project');
	   }
	});

var isIE11 = !!(navigator.userAgent.match(/Trident/) && navigator.userAgent.match(/rv[ :]11/));


//========== HP TYPING ==========//
// values to keep track of the number of letters typed, which quote to use. etc. Don't change these values.
var i = 0,
    a = 0,
    isBackspacing = false,
    isParagraph = false;

// Typerwrite text content. Use a pipe to indicate the start of the second line "|".
var textArray = [
  "Vector Control|Saving Lives"
];

// Speed (in milliseconds) of typing.
var speedForward = 150, //Typing Speed
    speedWait = 1000, // Wait between typing and backspacing
    speedBetweenLines = 500, //Wait between first and second lines
    speedBackspace = 25; //Backspace Speed

//Run the loop


function typeWriter(id, ar) {
  var element = $("#" + id),
      aString = ar[a],
      eHeader = element.children("h1"), //Header element
      eParagraph = element.children("h2"); //Subheader element

  // Determine if animation should be typing or backspacing
  if (!isBackspacing) {

    // If full string hasn't yet been typed out, continue typing
    if (i < aString.length) {

      // If character about to be typed is a pipe, switch to second line and continue.
      if (aString.charAt(i) == "|") {
        isParagraph = true;
        eHeader.removeClass("cursor");
        eParagraph.addClass("cursor");
        i++;
        setTimeout(function(){ typeWriter(id, ar); }, speedBetweenLines);

      // If character isn't a pipe, continue typing.
      } else {
        // Type header or subheader depending on whether pipe has been detected
        if (!isParagraph) {
          eHeader.text(eHeader.text() + aString.charAt(i));
        } else {
          eParagraph.text(eParagraph.text() + aString.charAt(i));
        }
        i++;
        setTimeout(function(){ typeWriter(id, ar); }, speedForward);
      }

    // If full string has been typed, switch to backspace mode.
    } else if (i == aString.length) {

      isBackspacing = true;
      setTimeout(function(){ typeWriter(id, ar); }, speedWait);

    }
  }
}


var waypoints = $('.output').waypoint(function(direction) {
   typeWriter("output", textArray);
}, {
  offset: '90%'
})

$('#searchsubmit').text('Search');

$('#myvideo').click(function() {
	$(this).trigger( $('video').prop('paused') ? 'play' : 'pause');
});

$('#myvideo').on('keydown', function(event) {
	if(event.keyCode == 13 || event.keyCode == 110) {
		$('#myvideo').trigger( $('video').prop('paused') ? 'play' : 'pause');
	}
});

});


