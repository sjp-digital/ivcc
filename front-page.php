<?php get_header(); ?>

	<div class="homepage-intro-section hp-intro">
		<div class="left-col">
			<div class="output" id="output">
				<h1 class="cursor"></h1>
				<h2></h2>
				<!-- <h1 id="typewriter"><span class="typed-one">Vector Control</span><span class="typed-two">Saving Lives</span></h1> -->
			</div>

			<div class="hp-filters">
				<button class="filter active-filter" data-related="projects" tabindex="0" aria-label="View Projects">Projects</button>
				<button class="filter" data-related="who-we-work-with" tabindex="0" aria-label="View Who We Work With">Who We Work With</button>
				<button class="filter" data-related="market-access" tabindex="0" aria-label="View Market Access">Market Access</button>
			</div>
		</div>
		<div class="right-col-content">
			<div class="intro-content active-content" data-related="projects">
				<p><?php the_field('projects_introductory_content', $post->ID); ?></p>
			</div>
			<div class="intro-content" data-related="market-access">
				<p><?php the_field('market_access_introductory_content', $post->ID); ?></p>
			</div>
			<div class="intro-content" data-related="who-we-work-with">
				<p><?php the_field('who_we_work_with_introductory_content', $post->ID); ?></p>
			</div>
		</div>
		<hr>
	</div>

	<div class="maps">
		<?php include svg('projects.php'); ?>
		<a href="#mapend" class="maps__skip">Skip Map</a>
		<div class="map projects active-map" data-related="projects">
			<?php while ( have_rows('add_location', $post->ID) ) : the_row(); ?>
				<? $location = get_sub_field('location', $post->ID); ?>
				<div class="location <? the_sub_field('size_of_marker', $post->ID) ?> <?= $location['value']; ?>" tabindex="0" aria-label="Location <?= $location['label']; ?>">
					<div class="location-details">
						<h4><?= $location['label']; ?></h4>

						<? while ( have_rows('body_of_work', $post->ID) ) : the_row(); ?>
							<div class="body-of-work">
								<h5><? the_sub_field('body_of_work_title', $post->ID); ?></h5>

								<? while ( have_rows('site', $post->ID) ) : the_row(); ?>
									<p><a href="<? the_sub_field('site_link', $post->ID); ?>"><? the_sub_field('site_title', $post->ID); ?></a></p>

								<? endwhile; ?>
							</div>
						<? endwhile; ?>
					</div>
					<div class="location-marker"></div>
				</div>
			<? endwhile; ?>
			<div id="mapend" tabindex="0"></div>
		</div>

		<div class="map who-we-work-with" data-related="who-we-work-with">
			<? while ( have_rows('add_who_we_work_with_location', $post->ID) ) : the_row(); ?>
				<? $location = get_sub_field('location', $post->ID); ?>
				<div class="location <? the_sub_field('size_of_marker', $post->ID) ?> <?= $location['value']; ?>" tabindex="0" aria-label="Location <?= $location['label']; ?>">
					<div class="location-details">
						<? while ( have_rows('organisation_type', $post->ID) ) : the_row(); ?>
							<div class="body-of-work">
								<? if(get_sub_field('specific_location', $post->ID)): ?>
									<h4><? the_sub_field('specific_location', $post->ID); ?></h4>
								<? endif; ?>
								<h5><? the_sub_field('organisation_type', $post->ID); ?></h5>

								<? while ( have_rows('organisation', $post->ID) ) : the_row(); ?>
									<p><a href="<? the_sub_field('organisation_link', $post->ID); ?>"><? the_sub_field('organisation_title', $post->ID); ?></a></p>
								<? endwhile; ?>
							</div>
						<? endwhile; ?>
					</div>
					<div class="location-marker"></div>
				</div>
			<? endwhile; ?>
		</div>

		<div class="map market-access" data-related="market-access">
			<? while ( have_rows('add_market_access_location', $post->ID) ) : the_row(); ?>
				<? $location = get_sub_field('location', $post->ID); ?>
				<div class="location <? the_sub_field('size_of_marker', $post->ID) ?> <?= $location['value']; ?>" tabindex="0" aria-label="Location <?= $location['label']; ?>">
					<div class="location-details">
						<h4><?= $location['label']; ?></h4>

						<? while ( have_rows('body_of_work', $post->ID) ) : the_row(); ?>
							<? $bodyofworktitle = get_sub_field('body_of_work_title', $post->ID); ?>
							<div class="body-of-work <?= $bodyofworktitle['value']; ?>">
								<h5><?= $bodyofworktitle['label']; ?></h5>
								<p><a href="<? the_sub_field('site_link', $post->ID); ?>"><? the_sub_field('site_title', $post->ID); ?></a></p>
							</div>
						<? endwhile; ?>
					</div>
					<div class="location-marker<? while ( have_rows('body_of_work', $post->ID) ) : the_row(); $bodyofworktitle = get_sub_field('body_of_work_title', $post->ID); echo ' '.$bodyofworktitle['value']; endwhile; ?>"></div>
				</div>
			<? endwhile; ?>
		</div>
	</div>

	<?php include('grid/hp-grid.php'); ?>

	<div class="funding-partners wrap">
		<h2>Funding Partners</h2>

		<div class="funding-partners-grid">
			<?php while ( have_rows('funding_partner_logos') ) : the_row(); ?>
			<?php if(get_sub_field('link')): ?>
				<a href="<?php the_sub_field('link'); ?>" target="_blank" class="partner-image">
			<?php else: ?>
				<a class="partner-image">
			<?php endif; ?>
				<?php $image = get_sub_field('logo'); ?>
				<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
			</a>
			<?php endwhile; ?>
		</div>
	</div>


<?php get_footer(); ?>
