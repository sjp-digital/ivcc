<?php
/*
	Template Name: Market Access Overview
*/
?>

<?php get_header(); ?>

	<div class="section_introduction market-access-introduction wrap">
		<div class="left-col">
			<div class="market-access-intro-content" data-related="general">
				<h1><?php the_title(); ?></h1>

				<div class="content">
					<?php the_field('introductory_content', $post->ID); ?>
				</div>
			</div>
			<div class="market-access-intro-content with-logo" data-related="ngenirs">
				<img class="section-logo" src="<?php image('ngenirs-logo.svg') ?>">

				<div class="content">
					<?php the_field('ngenirs_introductory_content', $post->ID); ?>
				</div>
			</div>
			<div class="market-access-intro-content with-logo" data-related="nnp">
				<img class="section-logo" src="<?php image('nnp-logo.svg') ?>">

				<div class="content">
					<?php the_field('nnp_introductory_content', $post->ID); ?>
				</div>
			</div>
		</div>

		<div class="map active-dots active-map market-access">
			<?php include svg('projects.php'); ?>

			<? while ( have_rows('add_market_access_location', 6) ) : the_row(); ?>
				<? $location = get_sub_field('location', 6); ?>
				<div class="location <? the_sub_field('size_of_marker', 6) ?> <?= $location['value']; ?>">
					<div class="location-details">
						<h4><?= $location['label']; ?></h4>

						<? while ( have_rows('body_of_work', 6) ) : the_row(); ?>
							<? $bodyofworktitle = get_sub_field('body_of_work_title', 6); ?>
							<div class="body-of-work <?= $bodyofworktitle['value']; ?>">
								<h5><?= $bodyofworktitle['label']; ?></h5>
								<p><a href="<? the_sub_field('site_link', 6); ?>"><? the_sub_field('site_title', 6); ?></a></p>
							</div>
						<? endwhile; ?>
					</div>
					<div class="location-marker<? while ( have_rows('body_of_work', 6) ) : the_row(); $bodyofworktitle = get_sub_field('body_of_work_title', 6); echo ' '.$bodyofworktitle['value']; endwhile; ?>"></div>
				</div>
			<? endwhile; ?>		</div>
	</div>

	<div class="grid-container">
		<div class="wrap">
			<? if( have_rows('grid_category', $post->ID) ): ?>
				<div class="filters-container mobile-filter updated-filters market-access-filters">
					<div class="mobile-filter-trigger">
						<p>Filter By:</p>
						<img src="<?php image('red-chevron.svg') ?>">
					</div>
					<ul>
						<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
							<? $cat_title = get_sub_field('category_title',$post->ID); ?>
							<? $cat_title = str_replace(' ', '-', $cat_title); ?>
							<? $cat_title = strtolower($cat_title); ?>

							<li class="filter <?= $cat_title; ?>-filter" grid-related="<?= $cat_title; ?>"><? the_sub_field('category_title', $post->ID); ?></li>
						<?php endwhile; ?>
					</ul>
				</div>
			<? endif; ?>

			<? if( have_rows('grid_category', $post->ID) ): ?>
				<div class="filters-container desktop-filters updated-filters market-access-filters">
					<p>FILTER:</p>
					<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
						<? $cat_title = get_sub_field('category_title',$post->ID); ?>
						<? $cat_title = str_replace(' ', '-', $cat_title); ?>
						<? $cat_title = strtolower($cat_title); ?>
						<div class="single-filter-group-container">
							<div class="filter <?= $cat_title; ?>-filter <?php the_sub_field('filter_colour', $post->ID) ?>" grid-related="<?= $cat_title; ?>"><? the_sub_field('category_title', $post->ID); ?></div>
						</div>
					<?php endwhile; ?>
				</div>
			<? endif; ?>
		</div>

		<? if( have_rows('grid_category', $post->ID) ): ?>
			<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
				<? $cat_title = get_sub_field('category_title',$post->ID); ?>
				<? $cat_title = str_replace(' ', '-', $cat_title); ?>
				<? $cat_title = strtolower($cat_title); ?>
				<div class="grid-category-container <?= $cat_title; ?>" grid-related="<?= $cat_title; ?>">
					<? if(get_sub_field('category_image', $post->ID)): ?>
						<div class="wrap">
							<img class="category-image" alt="<? the_sub_field('category_title',$post->ID); ?> Image" src="<? the_sub_field('category_image', $post->ID); ?>">
						</div>
					<? endif; ?>
					<div class="grid wrap">
						<div class="grid-sizer"></div>
						<div class="gutter-sizer"></div>
						<? the_sub_field('category_title', $post->ID); ?>
						<?php $posts = get_sub_field('inner_grid_selection', $post->ID); ?>
						<?php $posttype = 'any'; ?>
						<?php get_overview_grid($posttype, $posts) ?>
					</div>
				</div>
			<? endwhile; ?>
		<? endif; ?>

	</div>
</div>

<?php get_footer(); ?>
