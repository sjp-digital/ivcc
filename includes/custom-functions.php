<?php

# =========== STYLE LOGIN =========== #
function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/dist/login-style.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

# =========== GET GRID FROM TAGS =========== #
	function get_related_grid($posts, $tags, $postID) {
		if($posts > 0):
			$ordertype = 'post__in';
		else:
			$ordertype = 'published';
		endif;

		$args = array(
			'post__not_in' 		=> array($postID),
			'post_status'       => 'publish',
			'post_type'         => 'any',
			'posts_per_page'    => 12,
			'tag__in'	=> 		$tags,
			'orderby'	=> $ordertype,
			'post__in' => $posts,
		);
		$query = new WP_Query($args);

		$i = 1;
		$col_no = 1;

		foreach($query->posts as $post):
			setup_postdata( $post );
			$col_content_id = $post->ID;

			$posttags = get_the_tags($col_content_id);

			switch ($col_no):
				case 4:
			      $col_no = '1';
			        break;
			endswitch;

			switch ($i):
		    	case 1:
		    		$row_format = 'row-a';
		    		break;
		    	case 4:
		    		$row_format = 'row-b';
		    		break;
		    	case 7:
		    	 	$row_format = 'row-c';
		    	 	break;
		    	case 9:
		    		$i = '0';
		    	break;
			endswitch;

			if(get_post_type($col_content_id) == 'page'):

				include gridelement('col-page.php');

			elseif(get_post_type($col_content_id) == 'events'):

				include gridelement('col-events.php');

			elseif(get_post_type($col_content_id) == 'resources'):

				include gridelement('col-resources.php');

			elseif(get_post_type($col_content_id) == 'post'):

				include gridelement('col-post.php');

			elseif(get_post_type($col_content_id) == 'research-development'):

				include gridelement('col-post.php');

			elseif((get_the_title($col_content_id) == 'NgenIRS') || get_the_title($col_content_id) =='New Nets Project (NNP)'):

				include gridelement('col-bespoke.php');

			else:

				include gridelement('col-post.php');

			endif;

			$col_no++;
			$i++;

		endforeach;
		wp_reset_query();
	}

# =========== GET OVERVIEW PAGE GRID =========== #
	function get_overview_grid($posttype, $posts) {
		$order = 'DESC';
		$metakey = '';
		$ordertype = '';
		$tag = '';

		if($posttype == 'events'):
			$ordertype = 'meta_value';
	       	$metakey = 'start_date';
	        $order = 'ASC';

	   	elseif($posttype == 'market-access-lower'):
	    	$posttype = 'any';
	    	$tag = array(21, 34);

	    elseif($posttype == 'resources'):
	    	$order = 'DESC';
	    	$ordertype = 'published';

	    elseif($posttype == 'post'):
	    	$ordertype = 'published';

		else:
        	$ordertype = 'post__in';
		endif;

		$args = array(
			'post_status'       => 'publish',
			'post_type'         => $posttype,
			'posts_per_page'    => -1,
			'post__in' 			=> $posts,
			'orderby'			=> $ordertype,
			'order'				=> $order,
			'meta_key'			=> $metakey,
			'tag__in' 			=> $tag
		);

		$query = new WP_Query($args);

			$i = 1;
			$col_no = 1;

		if ( $query->have_posts() ):
			foreach($query->posts as $post):
				setup_postdata( $post );
				$col_content_id = $post->ID;

				if($posttype == 'post'):
					$post_categories = get_the_terms($col_content_id, 'category');
				elseif($posttype == 'research-development'):
					$post_categories = get_the_terms($col_content_id, $posttype.'-cats');
				else:
					$post_categories = get_the_terms($col_content_id, $posttype.'-categories');
				endif;

				$posttags = get_the_tags($col_content_id);

				switch ($col_no):
					case 4:
				      $col_no = '1';
				        break;
				endswitch;

				switch ($i):
			    	case 1:
			    		$row_format = 'row-a';
			    		break;
			    	case 4:
			    		$row_format = 'row-b';
			    		break;
			    	case 7:
			    	 	$row_format = 'row-c';
			    	 	break;
			    	case 9:
			    		$i = '0';
			    	break;
				endswitch;

				if(get_post_type($col_content_id) == 'page'):

					include gridelement('col-page.php');

				elseif(get_post_type($col_content_id) == 'events'):

					include gridelement('col-events.php');

				elseif(get_post_type($col_content_id) == 'resources'):

					include gridelement('col-resources.php');

				elseif(get_post_type($col_content_id) == 'post'):

					include gridelement('col-post.php');

				elseif(get_post_type($col_content_id) == 'events'):

					include gridelement('col-events.php');

				elseif(get_post_type($col_content_id) == 'careers'):

					include gridelement('col-vacancy.php');

				elseif((get_the_title($col_content_id) == 'NgenIRS') || get_the_title($col_content_id) =='New Nets Project (NNP)'):

					include gridelement('col-bespoke.php');

				else:

					include gridelement('col-post.php');

				endif;

				$col_no++;
				$i++;

			endforeach;
		else:
			if($posttype == 'careers'): ?>
				<p class="no-posts">No positions currently available, check back soon or join our newsletter for more information.</p>
			<? else: ?>
				<p  class="no-posts">No posts available, check back soon</p>
			<? endif;
		endif;
		wp_reset_query();
	}

# =========== GET RESEARCH PAPERS =========== #
	function get_research_papers($associated_papers) {
		$args = array(
			'post_status'       => 'publish',
			'post_type'         => 'downloads',
			'posts_per_page'    => -1,
			'post__in' =>		$associated_papers,
			'orderby' => 'post__in'
		);

		$query = new WP_Query($args);

			foreach(array_chunk($query->posts, 6) as $item ): ?>
				<div class="inner-container">
					<?php foreach($item as $post): ?>
						<?php setup_postdata( $post ); ?>
						<?php $attachment_id = get_field('file', $post->ID);
						$file_url = wp_get_attachment_url( $attachment_id ); ?>

						<? $downloadablefile = get_field('downloadable_file', $post->ID); ?>

						<div class="research-paper">
							<div class="paper-content">
								<img class="pdf-icon" src="<?php image('icon-paper.svg') ?>">
								<strong><?php echo get_the_title($post->ID); ?></strong>
								<?php the_field('paper_description', $post->ID); ?>
							</div>
							<div class="paper-actions">
								<? if($downloadablefile): ?>
									<div class="download resource-download-element"><a class="research-download" href="<?php echo $downloadablefile; ?>" download target="blank">DOWNLOAD <img alt="Download" src="<?php image('icon-download.svg') ?>"></a></div>
								<? else: ?>
									<div class="view resource-download-element" data-remodal-target="ajax-modal" postid="<?= $post->ID; ?>">VIEW</div>
									<div class="download resource-download-element"><a class="research-download" href="<?php echo $file_url; ?>" download target="blank">DOWNLOAD <img alt="Download" src="<?php image('icon-download.svg') ?>"></a></div>
								<? endif; ?>
							</div>
						</div>
					<? endforeach; ?>
				</div>
		<? endforeach;

		wp_reset_query();
	}

# =========== GET EVENT POINTS =========== #
	function event_points() {
		$args = array(
			'post_status'       => 'publish',
			'post_type'         => 'events',
			'posts_per_page'    => '-1',
			  'meta_query' => array(
			    array(
			      'key' => 'virtual_event',
			      'value' => '1',
			      'compare' => '!=' // not really needed, this is the default
			    )
			)
		);

		$query = new WP_Query($args);

		foreach($query->posts as $post): ?>
			<?php setup_postdata( $post ); ?>
			<?php $originalDate = get_field('start_date', $post->ID);
			$endDate = get_field('end_date', $post->ID);
			$startdisplayDate = date("d F", strtotime($originalDate));
			$enddisplayDate = date("d F", strtotime($endDate));
			$visibilitystartDate = date("Y-m", strtotime($originalDate));
			$visibilityendDate = date("Y-m", strtotime($endDate));
			$relatedDate = date("Y-m-d", strtotime($originalDate));
			$relatedEndDate = date("Y-m-d", strtotime($endDate)) ?>
			<?php $location = get_field('event_location', $post->ID) ?>

			<div class="point location-<?php echo $location['value']; ?>" location="<?php echo $location['value']; ?>" visibility-start-date="<?php echo $visibilitystartDate ?>" visibility-end-date="<?php echo $visibilityendDate ?>" date-related="<?php echo $relatedDate ?>"  end-date-related="<?php echo $relatedEndDate ?>">
				<div class="point-marker"></div>
				<div class="point-details">
					<a href="<?php echo get_permalink($post->ID); ?>">
					<strong><?php echo get_the_title($post->ID) ?></strong>
					<span><?php echo $location['label']; ?></span>
					<? if($startdisplayDate == $enddisplayDate) { ?>
						<span><?php echo $startdisplayDate; ?></span></a>
					<?}else { ?>
						<span><?php echo $startdisplayDate; ?> - <?php echo $enddisplayDate; ?></span></a>
					<? } ?>

				</div>
			</div>

		<?php endforeach;

		wp_reset_query();
	}

# =========== GET ESACS BLOCKS=========== #
	function get_current_esac_blocks() {
		$args = array(
			'post_status'       => 'publish',
			'post_type'         => 'research-development',
			'posts_per_page'    => '-1',
			'orderby'=> 'title',
			'post_parent' => '455',
			'order' => 'ASC',
			'meta_key'		=> 'esacs_status',
			'meta_value'	=> 1
   			);

		$query = new WP_Query($args);

		foreach($query->posts as $post): ?>
			<?php setup_postdata( $post ); ?>
				<div class="esac-block">
					<a href="<?php echo get_permalink($post->ID); ?>">
						<h3><?php echo get_the_title($post->ID); ?></h3>
						<hr>
						<p><?php the_field('post_excerpt', $post->ID) ?></p>
					</a>
				</div>
		<?php endforeach;

		wp_reset_query();
	}

	function get_legacy_esac_blocks() {
		$args = array(
			'post_status'       => 'publish',
			'post_type'         => 'research-development',
			'posts_per_page'    => '-1',
			'orderby'=> 'title',
			'order' => 'DESC',
			'meta_key'		=> 'esacs_status',
			'meta_value'	=> 0
		);

		$query = new WP_Query($args);

		foreach($query->posts as $post): ?>
			<?php setup_postdata( $post ); ?>
				<div class="esac-block legacy">
					<a href="<?php echo get_permalink($post->ID); ?>">
						<h3><?php echo get_the_title($post->ID); ?></h3>
						<hr>
						<p><?php the_field('post_excerpt', $post->ID) ?></p>
					</a>
				</div>
		<?php endforeach;

		wp_reset_query();
	}

# =========== OUTPUT HIERARCHICALY =========== #
	function sort_terms_hierarchicaly(Array &$cats, Array &$into, $parentId = 0) {
		foreach ($cats as $i => $cat) {
			if ($cat->parent == $parentId) {
				$into[$cat->term_id] = $cat;
				unset($cats[$i]);
			}
		}

		foreach ($into as $topCat) {
			$topCat->children = array();
			sort_terms_hierarchicaly($cats, $topCat->children, $topCat->term_id);
		}
	}

# =========== ADD ACF OPTIONS =========== #
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' 	=> 'Site General Settings',
			'menu_title'	=> 'Site Settings',
			'menu_slug' 	=> 'site-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}

# =========== REMOVE EMOJIS =========== #
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

# =========== MOVE YOAST TO BOTTOM =========== #
	function yoasttobottom() {
		return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

# =========== SOCIAL SHARE GENERATOR =========== #
	function social_share($types, $post) {
	$url = urlencode(get_permalink($post->ID));
	$title = urlencode($post->post_title);
	$description = urlencode(get_the_excerpt($post));
	$image = get_the_post_thumbnail_url($post->ID, 'full');
	$js = "onclick='javascript:window.open(this.href,\" \", \"menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;\"";

	?>

	<div class="social-share">
		<?php
			foreach($types as $t):
				switch($t):
					case 'f': ?>
						<a class="social-share__link social-share__link--facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?=$url;?>&amp;title=<?=$title;?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php image('facebook-share.svg') ?>" alt="Facebook Share"></a>
					<?php break;
					case 't'; ?>
						<a class="social-share__link social-share__link--twitter" href="http://twitter.com/intent/tweet?status=<?=$description." ".$url;?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php image('twitter-share.svg') ?>" alt="Twitter Share"></a>
					<?php break;
					case 'p'; ?>
						<a class="social-share__link social-share__link--pinterest" href="https://pinterest.com/pin/create/button/?url=<?=$url;?>&media=<?=$image;?>&description=<?=$description;?>" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=800,width=900');return false;"><i class="fab fa-pinterest"></i></a>
					<?php break;

					case 'g'; ?>
						<a class="social-share__link social-share__link--googleplus" href="https://plus.google.com/share?url=<?=$url;?>" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fab fa-google-plus-g"></i></a>
					<?php break;

					case 'l'; ?>
						<a class="social-share__link social-share__link--linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?=$url;?>&summary=<?=$description;?>" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=800,width=900');return false;"><img src="<?php image('linkedin-share.svg') ?>" alt="Linkedin Share"></a>
					<?php break;

				endswitch;
			endforeach;
		?>
	</div> <?php
	}

# =========== REMOVE EXCERPT LINK =========== #
	function new_excerpt_more($more) {
	  global $post;
	  remove_filter('excerpt_more', 'new_excerpt_more');
	  return '';
	}
	add_filter('excerpt_more','new_excerpt_more',11);

# =========== BUILD EVENTS CALENDAR =========== #
	function build_calendar($month,$year) {

		 // Create array containing abbreviations of days of week.
		 $daysOfWeek = array('M','T','W','T','F','S','S');

		 // What is the first day of the month in question?
		 $firstDayOfMonth = mktime(0,0,0,$month,1,$year);

		 // How many days does this month contain?
		 $numberDays = date('t',$firstDayOfMonth);

		 // Retrieve some information about the first day of the
		 // month in question.
		 $dateComponents = getdate($firstDayOfMonth);

		 // What is the name of the month in question?
		 $monthName = $dateComponents['month'];

		 // What is the index value (0-6) of the first day of the
		 // month in question.
		 $dayOfWeek = $dateComponents['wday'];

		 // Create the table tag opener and day headers

		 $calendar = "<table class='calendar'>";
		 $calendar .= "<tr>";

		 // Create the rest of the calendar

		 // Initiate the day counter, starting with the 1st.

		 $currentDay = 1;

		 $calendar .= "</tr><tr>";

		 // The variable $dayOfWeek is used to
		 // ensure that the calendar
		 // display consists of exactly 7 columns.

		$dayOfWeek = $dateComponents['wday'] - 1;
		// fix for monday start
		if ($dayOfWeek < 0) {
			$dayOfWeek = 6;
		}

		 if ($dayOfWeek > 0) {
			  $calendar .= "<td colspan='$dayOfWeek'>&nbsp;</td>";
		 }

		 $month = str_pad($month, 2, "0", STR_PAD_LEFT);

		 while ($currentDay <= $numberDays) {

			  // Seventh column (Saturday) reached. Start a new row.

			  if ($dayOfWeek == 7) {

				   $dayOfWeek = 0;
				   $calendar .= "</tr><tr>";

			  }

			  $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);

			  $date = "$year-$month-$currentDayRel";

			  $calendar .= "<td><div class='day' date-related='$date'>$currentDay</div></td>";

			  // Increment counters

			  $currentDay++;
			  $dayOfWeek++;
		 }

		 // Complete the row of the last week in month, if necessary

		 if ($dayOfWeek != 7) {

			  $remainingDays = 7 - $dayOfWeek;
			  $calendar .= "<td colspan='$remainingDays'>&nbsp;</td>";

		 }

		 $calendar .= "</tr>";

		 $calendar .= "</table>";

		 return $calendar;

	}

	$onths = array();
	$currentMonth = (int)date('m');

	for ($x = $currentMonth; $x < $currentMonth + 37; $x++) {
		$months[] = date('n,Y', mktime(0, 0, 0, $x, 1));
	}

# =========== CATEGORY IS ALWAYS IN PARENT CATEGORY =========== #
	add_action('save_post', 'assign_parent_terms', 10, 2);

	function assign_parent_terms($post_id, $post){
	    $arrayPostTypeAllowed = array('vector-control', 'market-access');
	    $arrayTermsAllowed = array('vector-control-categories', 'market-access-categories');

	    //Check post type
	    if(!in_array($post->post_type, $arrayPostTypeAllowed)){
	        return $post_id;
	    }else{

	        // get all assigned terms
	        foreach($arrayTermsAllowed AS $t_name){
	            $terms = wp_get_post_terms($post_id, $t_name );

	            foreach($terms as $term){

	                while($term->parent != 0 && !has_term( $term->parent, $t_name, $post )){

	                    // move upward until we get to 0 level terms
	                    wp_set_post_terms($post_id, array($term->parent), $t_name, true);
	                    $term = get_term($term->parent, $t_name);
	                }
	            }
	        }
	    }
	}

# =========== AJAX RESOURCE LIBRARY =========== #
	function add_ajax_support(){
		wp_enqueue_script( 'add_ajax_support', get_stylesheet_directory_uri() .'/library/js/libs/resource-content.js', 'jquery', true);
		wp_enqueue_script( 'add_ajax_support_team', get_stylesheet_directory_uri() .'/library/js/libs/teamcontent.js', 'jquery', true);
		wp_localize_script( 'add_ajax_support', 'ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	}

	add_action('wp_head','add_ajax_support');
	add_action( 'wp_ajax_nopriv_get_resource_content', 'get_resource_content' );
	add_action( 'wp_ajax_nopriv_get_team_content', 'get_team_content' );
	add_action( 'wp_ajax_nopriv_get_download_content', 'get_download_content' );

	function get_resource_content() {
		$postid = $_REQUEST['postid'];

		$args = array(
			'p' 				=> $postid,
			'post_status'    	=> 'publish',
			'post_type'        	=> 'resources',
			'posts_per_page'	=> '-1',
		);

		$query = new WP_Query($args);
		foreach($query->posts as $post):
			setup_postdata($post);

			$col_categories = get_the_terms($post->ID, 'resources-categories');
			$post_format = $col_categories[0]->slug;

			if( in_array($post_format, array('annual-reports','newsletter', 'policies', 'publications', 'presentations', 'zero-by-40', 'ngenirs-evidence', 'ambassador-pack'), true ) ):
				$fileid = get_field('select_file', $post->ID);
				$downloadablefile = get_field('downloadable_file', $fileid);

				$attachment_id = get_field('file', $fileid);
				$url = wp_get_attachment_url( $attachment_id ); ?>

				<div class="left-col pdf-details">
					<h3><?= get_the_title($post->ID); ?></h3>

					<? if($downloadablefile): ?>
						<a class="btn view-btn" href="<?php echo $downloadablefile; ?>" target="_blank">Download Resource</a>
						<a class="btn download-btn" href="<?php echo $downloadablefile; ?>" download>Download Resource</a>
					<? else: ?>
						<a class="btn view-btn" href="<?php echo $url; ?>" target="_blank">View Resource</a>
						<a class="btn download-btn" href="<?php echo $url; ?>" download>Download Resource</a>
					<? endif; ?>

					<?php if(get_field('pdf_description', $post->ID)): ?>
						<?php the_field('pdf_description', $post->ID); ?>
					<?php endif; ?>
				</div>

				<div class="right-col pdf-viewer">
					<embed src="<?php echo $url; ?>#view=FitH&view=fitW" type="application/pdf" width="100%" height="600px" />
				</div>

			<?php elseif ($post_format == 'videos'): ?>

				<?php if(get_field('video_description', $post->ID)): ?>
					<div class="left-col">
						<h3><?php echo get_the_title($post->ID); ?></h3>
						<?php the_field('video_description', $post->ID); ?>
					</div>
				<?php endif; ?>

				<div class="right-col video-container">
					<? $videourl = get_field('video_url', $post->ID); ?>
					<?php $embed_code = wp_oembed_get($videourl); ?>

					<? echo $embed_code; ?>
				</div>

			<?php elseif ($post_format == 'images'):
				$images = get_field('gallery', $post->ID);
				$size = 'full'; ?>

				<?php if(get_field('gallery_description', $post->ID)): ?>
					<div class="left-col">
						<h3><?php echo get_the_title($post->ID); ?></h3>
						<?php the_field('gallery_description', $post->ID); ?>
					</div>
				<?php endif; ?>

				<div class="right-col gallery-container">
					<?php foreach( $images as $image ): ?>
						<a rel="lightbox" href="<?php echo $image['url']; ?>"><img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>"></a>
					<?php endforeach; ?>
				</div>

		 	<?php endif;

		endforeach;
	   die();
	}
	add_action( 'wp_ajax_get_resource_content', 'get_resource_content' );

# =========== AJAX DOWNLOADS =========== #
	function get_download_content() {
		$postid = $_REQUEST['postid'];

		$args = array(
			'p' 				=> $postid,
			'post_status'    	=> 'publish',
			'post_type'        	=> 'downloads',
			'posts_per_page'	=> '-1',
		);

		$query = new WP_Query($args);
		foreach($query->posts as $post):
			setup_postdata($post);
			$attachment_id = get_field('file', $post->ID);
			$file_url = wp_get_attachment_url( $attachment_id ); ?>

			<div class="left-col pdf-details">
				<h3><?php echo get_the_title($post->ID); ?></h3>
				<a class="btn" href="<?php echo $file_url; ?>" download>Download</a>
				<?php if(get_field('paperget_download_content_description', $post->ID)): ?>
					<?php the_field('paper_description', $post->ID); ?>
				<?php endif; ?>
			</div>

			<div class="right-col pdf-viewer">
				<embed src="<?php echo $file_url; ?>" type="application/pdf" width="100%" height="600px" />
			</div>

		<?php endforeach;
	   die();
	}
	add_action( 'wp_ajax_get_download_content', 'get_download_content' );

# =========== AJAX TEAM =========== #
	function get_team_content() {
		$postid = $_REQUEST['postid'];

		$args = array(
			'p' 				=> $postid,
			'post_status'    	=> 'publish',
			'post_type'        	=> 'personnel',
			'posts_per_page'	=> '-1',
		);

		$query = new WP_Query($args);
		foreach($query->posts as $post): ?>
			<?php $thumb_id = get_post_thumbnail_id($post->ID);
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
			$thumb_url = $thumb_url_array[0]; ?>
			<?php $personnel_status = get_field('internal_or_external_personnel', $post->ID) ?>

			<?php if($personnel_status == 'internal'): ?>
				<div class="left-col">
					<div class="profile-picture" style="background: url('<?php echo $thumb_url; ?>') center / cover;"></div>
					<p><strong><?php echo get_the_title($post->ID); ?></strong><br>
					<?php the_field('job_title', $post->ID); ?><br>
					<a href="mailto:<?php the_field('email_address', $post->ID); ?>"><?php the_field('email_address', $post->ID); ?></a></p>
				</div>
				<div class="right-col">
					<?php the_field('personnel_content', $post->ID); ?>
				</div>
			<?php else: ?>
				<div class="left-col">
					<div class="profile-picture" style="background: url('<?php echo $thumb_url; ?>') center / cover;"></div>
					<div class="mobile-details">
						<p><strong><?php echo get_the_title($post->ID); ?></strong><br>
						<?php the_field('company', $post->ID); ?><br>
						<?php the_field('job_title', $post->ID); ?><br>
						<a href="mailto:<?php the_field('email_address', $post->ID); ?>"><?php the_field('email_address', $post->ID); ?></a></p>
					</div>
				</div>
				<div class="right-col external-content">
					<p><strong><?php echo get_the_title($post->ID); ?></strong><br>
					<?php the_field('company', $post->ID); ?><br>
					<?php the_field('job_title', $post->ID); ?><br>
					<a href="mailto:<?php the_field('email_address', $post->ID); ?>"><?php the_field('email_address', $post->ID); ?></a></p>
				</div>
			<?php endif; ?>
		<? endforeach;
	   die();
	}
	add_action( 'wp_ajax_get_team_content', 'get_team_content' );

# =========== HIDE MENU BITS =========== #
	add_action('admin_head', 'hide_menu_items');

	function hide_menu_items() { ?>
		<style>
			#menu-posts-rl_gallery, .toplevel_page_responsive-lightbox-settings, #menu-comments { display: none; }
	  	</style>
	<?php }


# =========== SEARCH RESULTS =========== #
	function search_items() {
		if(isset($_GET['s'])) {
    		$search_results = $_GET['s'];
		}

		if(isset($_GET['filter'])) {
			$filter = $_GET['filter'];

			if($filter == 'page'):
				$filter = array('page', 'market-access', 'research-development', 'vector-control');
				$filter_status = 'page';
			endif;
		} else {
			$filter = "any";
		}

		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		$args = array(
			's' => $search_results,
			'post_status'    	=> 'publish',
			'posts_per_page'	=> '10',
			'paged'			=> $paged,
			'post_type'		=> $filter,
			'post__not_in'	=> array(6,527,878)
		);

		$query = new WP_Query($args);
		$count = $query->found_posts; ?>

		<div class="wrap">
			<? if($count <= 10): ?>
				<h1>Search results for: <?= $search_results; ?></h1>

			<? if(isset($_GET['filter'])): ?>
				<div class="search-filters filters-container desktop-filters">
				<p>FILTER BY</p>
					<a href="?s=<?php echo get_search_query(); ?>&filter=any" name="all" class="filter">ALL</a>
					<a href="?s=<?php echo get_search_query(); ?>&filter=page" name="page" class="filter<? if($filter_status == 'page'): ?> active-filter<? endif; ?>">PAGE</a>
					<a href="?s=<?php echo get_search_query(); ?>&filter=events" name="event" class="filter<? if($filter == 'events'): ?> active-filter<? endif; ?>">EVENT</a>
					<a href="?s=<?php echo get_search_query(); ?>&filter=post" name="news" class="filter<? if($filter == 'post'): ?> active-filter<? endif; ?>">NEWS</a>
					<a href="?s=<?php echo get_search_query(); ?>&filter=personnel" name="people" class="filter<? if($filter == 'personnel'): ?> active-filter<? endif; ?>">PEOPLE</a>
					<a href="?s=<?php echo get_search_query(); ?>&filter=resources" name="resources" class="filter<? if($filter == 'resources'): ?> active-filter<? endif; ?>">RESOURCES</a>
				</div>

			<div class="filters-container mobile-filter">
				<div class="mobile-filter-trigger">
					<p>Filter By:</p>
					<img src="<?php image('red-chevron.svg') ?>">
				</div>
				<ul>
					<li><a href="?s=<?php echo get_search_query(); ?>&filter=any" class="filter">ALL</a></li>
					<li><a href="?s=<?php echo get_search_query(); ?>&filter=page" id="page" name="page" class="filter<? if($filter_status == 'page'): ?> active-filter<? endif; ?>">PAGE</a></li>
					<li><a href="?s=<?php echo get_search_query(); ?>&filter=events" id="event" name="event" class="filter<? if($filter == 'event'): ?> active-filter<? endif; ?>">EVENT</a></li>
					<li><a href="?s=<?php echo get_search_query(); ?>&filter=post" id="news" name="news" class="filter<? if($filter == 'news'): ?> active-filter<? endif; ?>">NEWS</a></li>
					<li><a href="?s=<?php echo get_search_query(); ?>&filter=personnel" id="people" name="people" class="filter<? if($filter == 'people'): ?> active-filter<? endif; ?>">PEOPLE</a></li>
				</ul>
			</div>
			<? endif; ?>

			<? else: ?>
				<h1>Search results for: <?= $search_results; ?></h1>
				<h5>To help you quickly find what you're looking for, the following filters will help refine your search:</h5>

			<div class="search-filters filters-container desktop-filters">
				<p>FILTER BY</p>

				<a href="?s=<?php echo get_search_query(); ?>&filter=any" class="filter active-filter">ALL</a>
				<a href="?s=<?php echo get_search_query(); ?>&filter=page" id="page" name="page" class="filter">PAGE</a>
				<a href="?s=<?php echo get_search_query(); ?>&filter=events" id="event" name="event" class="filter">EVENT</a>
				<a href="?s=<?php echo get_search_query(); ?>&filter=post" id="news" name="news" class="filter">NEWS</a>
				<a href="?s=<?php echo get_search_query(); ?>&filter=personnel" id="people" name="people" class="filter">PEOPLE</a>
			</div>

			<div class="filters-container mobile-filter">
				<div class="mobile-filter-trigger">
					<p>Filter By:</p>
					<img src="<?php image('red-chevron.svg') ?>">
				</div>
				<ul>
					<li><a href="?s=<?php echo get_search_query(); ?>&filter=any" class="filter active-filter">ALL</a></li>
					<li><a href="?s=<?php echo get_search_query(); ?>&filter=page" id="page" name="page" class="filter">PAGE</a></li>
					<li><a href="?s=<?php echo get_search_query(); ?>&filter=events" id="event" name="event" class="filter">EVENT</a></li>
					<li><a href="?s=<?php echo get_search_query(); ?>&filter=post" id="news" name="news" class="filter">NEWS</a></li>
					<li><a href="?s=<?php echo get_search_query(); ?>&filter=personnel" id="people" name="people" class="filter">PEOPLE</a></li>
				</ul>
			</div>

			<? endif; ?>
		</div>

		<? if(1 == $paged && $count >= 6) { ?>
			<h5 class="results-header wrap"><strong>TOP RESULTS</strong></h5>
		<? } ?>

		<div class="top-results results wrap">
		<? if ( $query->have_posts() ):
			$i = 0;

			foreach($query->posts as $post): ?>
				<? $posttype = get_post_type($post->ID) ?>

				<div class="search-result<? if($i < '5' && ($paged ==1)): ?> featured-results<? endif; ?>">
					<a href="<? if(($posttype ==  'resources') || ($posttype == 'personnel')): ?>#<? else: ?><?php echo get_the_permalink($post->ID) ?><? endif; ?>" class="<?= $posttype ?>" <? if(($posttype ==  'resources') || ($posttype == 'personnel')): ?>data-remodal-target="ajax-modal"<? endif; ?> postid="<?= $post->ID ?>">

						<? if($posttype ==  'resources'):
							$col_categories = get_the_terms($post->ID, 'resources-categories');
							if($col_categories):
								foreach( $col_categories as $col_category ):
								$colcat = $col_category->slug;

								switch ($colcat):
							    	case 'annual-reports': ?>
							    		<h4><?php echo get_the_title($post->ID); ?><img class="resource-icon" src="<? image('icon-annual-reports.svg') ?>" alt="Annual Reports Icon"></h4>
							    		<? break;
							    	case 'policies': ?>
							    		<h4><?php echo get_the_title($post->ID); ?><img class="resource-icon" src="<? image('icon-policies.svg') ?>" alt="Policies Icon"></h4>
							    		<? break;
							    	case 'publications': ?>
							    	 	<h4><?php echo get_the_title($post->ID); ?><img class="resource-icon" src="<? image('icon-publications.svg') ?>" alt="Publications Icon"></h4>
							    	 	<? break;
							    	 case 'videos': ?>
							    	 	<h4><?php echo get_the_title($post->ID); ?><img class="resource-icon" src="<? image('red-play-icon.svg') ?>" alt="Play Icon"></h4>
										<? break;
							    	 case 'images': ?>
							    	 	<h4><?php echo get_the_title($post->ID); ?><img class="resource-icon" src="<? image('icon-image.svg') ?>" alt="Image Icon"></h4>
							    	<? default: ?>
							    	 	<h4><?php echo get_the_title($post->ID); ?></h4>
								<? endswitch;
								endforeach;
								if(get_field('pdf_description', $post->ID)):
									$pdf_description = get_field('pdf_description', $post->ID); ?>
									<? $pdf_description = wp_strip_all_tags($pdf_description); ?>
									<p><?php echo wp_trim_words($pdf_description, 20)?></p>
								<? endif;
							endif;
						elseif(($posttype == 'page') || ($posttype == 'research-development') || ($posttype == 'vector-control') || ($posttype == 'market-access')  || ($posttype == 'zero-by-40')) : ?>
							<h4><?php echo get_the_title($post->ID); ?></h4>
							<? if( have_rows('blocks', $post->ID) ):
								while ( have_rows('blocks', $post->ID) ) : the_row();
									if( get_row_layout() == 'page_introduction' ):
										$intro_content = get_sub_field('page_introduction_content', $post->ID); ?>
										<? $intro_content = wp_strip_all_tags($intro_content); ?>
										<p><?php echo wp_trim_words($intro_content, 20)?></p>
									<? endif;
								endwhile;
							elseif(get_field('introductory_content', $post->ID)):
								$intro_content = get_field('introductory_content', $post->ID); ?>
								<? $intro_content = wp_strip_all_tags($intro_content); ?>
								<p><?php echo wp_trim_words($intro_content, 20)?></p>
							<? endif;

						elseif($posttype == 'events'): ?>
							<?php $originalDate = get_field('start_date', $post->ID);
							$startDate = date("d/m/Y", strtotime($originalDate));	?>

							<?php $endDate = get_field('end_date', $post->ID);
							$endDate = date("d/m/Y", strtotime($endDate));	?>
							<?php $location = get_field('event_location', $post->ID) ?>
							<h4><?php echo get_the_title($post->ID); ?></h4>
							<p><?php echo $startDate; ?> - <?php echo $endDate; ?><br><?php echo $location['label']; ?></p>
						<? elseif($posttype == 'post'): ?>
							<h4><?php echo get_the_title($post->ID); ?></h4>
							<p class="date"><?php echo get_the_date('d F Y', $post->ID) ?></p>
							<p><?php echo wp_trim_words($post->post_content, 20)?></p>
						<? elseif($posttype == 'personnel'): ?>
							<? $personnel_content = get_field('personnel_content', $post->ID); ?>
							<? $personnel_content = wp_strip_all_tags($personnel_content); ?>
							<h4><?php echo get_the_title($post->ID); ?></h4>
							<p><?php echo wp_trim_words($personnel_content, 20)?></p>
						<? endif; ?>
					</a>
				</div>

				<? if($count >= 6): ?>
					<? if($i == '4' && ($paged ==1)): ?>
						<h5 class="results-header lower-header"><strong>OTHER RESULTS</strong></h5>
					<? endif; ?>
				<? endif; ?>

				<? $i++; ?>
			<?php endforeach; ?>

			<? if($query->max_num_pages > 1): ?>
				<div class="pagination wrap">
		    		<?php echo paginate_links( array( 'prev_next' => false, 'total' => $query->max_num_pages)); ?>
				</div>
			<?php endif; ?>

		<?php else: ?>
			<p>Currently no results available for: <strong><?php echo $search_results ?></strong></p>
			<p>Please try again.</p>
		<?php endif; ?>
		</div>
	<? }


# =========== LATEST POST FOR HP GRID =========== #
	function latest_post($post_type) {
		if($post_type == 'events'):
			$today = date('Ymd');

		    $args = array(
				'post_status'    	=> 'publish',
				'post_type'        	=> 'events',
				'posts_per_page'	=> '1',
				'order'				=> 'desc',
				'meta_query' => array(
                    array(
                        'key' => 'start_date',
                        'value' => $today,
                        'compare' => '>='
                    ),
                ),
			);
	    else:
	    $args = array(
			'post_status'    	=> 'publish',
			'post_type'        	=> 'post',
			'posts_per_page'	=> '1',
			'orderby'			=> 'published',
			'order'				=> 'DESC',
		);
	    endif;

		$query = new WP_Query($args);

		foreach($query->posts as $post): ?>
			<? global $col_content_id; ?>
			<?php setup_postdata( $post ); ?>
			<? $col_content_id = $post->ID; ?>
		<?php endforeach;

		wp_reset_query();
	}

# =========== EXCLUDE FROM SEARCH =========== #
add_action( 'init', 'searchexclusion', 99 );

function searchexclusion() {
	global $wp_post_types;
    $wp_post_types['downloads']->exclude_from_search = true;
}

?>
