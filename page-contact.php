<?php
/*
	Template Name: Contact Page
*/
?>

<?php get_header(); ?>

<div class="contact-page-container">
	<div class="page-intro wrap">
		<h1>Contact IVCC</h1>
	</div>

	<div class="lower-contact-page">
		<div class="contact-container wrap">
			<?php echo do_shortcode('[contact-form-7 id="2913" title="General Enquiry Form"]'); ?>
		</div>

		<div class="details">
			<div class="wrap">
				<div class="map-container">
				<h2 class="h3">IVCC Location</h2>
				<p>Pembroke Place, Liverpool City Centre, UK</p>
				<?php the_field('google_map_link', $post->ID); ?>
			</div>
			<div class="details">
				<div class="top-row">
					<div class="address-container">
					<h2 class="h3">Address</h2>
					<address class="address"><?php the_field('address', 'option'); ?></address>
					<h2 class="h3" style="margin-top: 30px;">Social Media</h2>
						<div class="social-container">
							<?php if(get_field('facebook','option')): ?><a target="_blank"href="<?php the_field('facebook', 'option'); ?>" aria-label="Link to IVCC Facebook page"><i class="fa fa-facebook-square" aria-hidden="true"></i></a><?php endif; ?>
							<?php if(get_field('linkedin','option')): ?><a target="_blank"href="<?php the_field('linkedin', 'option'); ?>" aria-label="Link to IVCC LinkedIn page"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a><?php endif; ?>
							<?php if(get_field('twitter','option')): ?><a target="_blank"href="<?php the_field('twitter', 'option'); ?>" aria-label="Link to IVCC Twitter page"><i class="fa fa-twitter-square" aria-hidden="true"></i></a><?php endif; ?>
							<?php if(get_field('vimeo','option')): ?><a target="_blank"href="<?php the_field('vimeo', 'option'); ?>" aria-label="Link to IVCC Vimeo page"><i class="fa fa-vimeo-square" aria-hidden="true"></i></a><?php endif; ?>
							<?php if(get_field('youtube','option')): ?><a target="_blank"href="<?php the_field('youtube', 'option'); ?>" aria-label="Link to IVCC YouTube page"><i class="fa fa-youtube-square" aria-hidden="true"></i></a><?php endif; ?>
						</div>
					</div>


					<div class="media-information">
						<?php the_field('media_information', $post->ID); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
