<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<?php $posttags = get_the_tags($post->ID); ?>

	<div class="read-time-count single-post-container wrap">
		<div class="post-intro">
			<h1><?php the_title() ?></h1>
			<div class="page-info">
				<div class="date"><?php echo get_the_date('jS F Y') ?></div>
				<div class="read-time">READ TIME <strong><span class="eta"></span></strong> <img src="<?php image('read-time.svg') ?>"></div>
				<?php if($posttags): ?>
					<div class="tag-container">
						<?php foreach( $posttags as $tag ): ?>
							<a class="tag <? echo $tag->slug; ?>" href="<? url() ?>/?s=<?php echo $tag->slug; ?>">#<? echo $tag->name; ?></a>
		    			<?php endforeach; ?>
	    			</div>
				<?php endif; ?>

				<?php if(get_field('select_author')): ?>
					<?php $author_id = get_field('select_author', $post->ID); ?>
					<?php $thumb_id = get_post_thumbnail_id($author_id);
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
					$thumb_url = $thumb_url_array[0]; ?>

					<div class="author-info">
						<img class="author-pic" src="<?php echo $thumb_url; ?>">
						<div class="author-details">
							<strong>Author:</strong><br>
							<?php echo get_the_title($author_id); ?><br>
							<?php if(get_field('company', $author_id)):
								echo the_field('company', $author_id); ?><br>
							<? endif; ?>
							<a href="mailto:<?php the_field('email_address', $author_id) ?>"><?php the_field('email_address', $author_id); ?></a>
						</div>
					</div>
				<?php endif;?>
			</div>
		</div>

		<div class="post-inner">
			<div class="content">
			<?php $thumb_id = get_post_thumbnail_id($post->ID);
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', true);
			$thumb_url = $thumb_url_array[0]; ?>

			<img class="featured-image<? if(get_field('featured_image_caption', $post->ID)): ?> caption-present<? endif; ?>" alt="<?php the_title() ?>" src="<?php echo $thumb_url; ?>">
			<? if(get_field('featured_image_caption', $post->ID)): ?>
				<p class="image-caption"><? the_field('featured_image_caption', $post->ID); ?></p>
			<? endif; ?>

				<?php the_content(); ?>

				<?php social_share(array('f', 't', 'l'), $post); ?>
			</div>

			<div class="sidebar">
				<?php $thumb_id = get_post_thumbnail_id($post->ID);
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', true);
				$thumb_url = $thumb_url_array[0]; ?>
				<img class="featured-image<? if(get_field('featured_image_caption', $post->ID)): ?> caption-present<? endif; ?>" alt="<?php the_title() ?>" src="<?php echo $thumb_url; ?>">
				<? if(get_field('featured_image_caption', $post->ID)): ?>
					<p class="image-caption"><? the_field('featured_image_caption', $post->ID); ?></p>
				<? endif; ?>

				<?php if(get_field('downloads',$post->ID)): ?>
					<h4>Resources</h4>
					<?php $downloads = get_field('downloads',$post->ID); ?>

					<?php foreach($downloads as $download):
						$attachment_id = get_field('file', $download);
						$url = wp_get_attachment_url( $attachment_id );
						$filesize = filesize( get_attached_file( $attachment_id ) );
						$filesize = size_format($filesize);
						$path_info = pathinfo( get_attached_file( $attachment_id ) ); ?>

						<div class="article-download">
							<img class="pdf-icon" src="<?php image('article-download.svg') ?>">
							<div class="paper-content">
								<p><strong><?php echo get_the_title($download); ?></strong>
								Size <?php echo $filesize; ?>, <span><?php echo $path_info['extension']; ?></span></p>

								<div class="paper-actions">
									<div class="download"><a class="download-link" target="_blank" download href="<?php echo $url; ?>">DOWNLOAD <img alt="Download" src="<?php image('icon-download.svg') ?>"></a></div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>

<?php endwhile; ?>


<?php get_footer(); ?>
