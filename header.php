<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<!-- Google Tag Manager -->

		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

		})(window,document,'script','dataLayer','GTM-TF4DWP6');</script>

		<!-- End Google Tag Manager -->
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>



<!-- Hotjar Tracking Code for https://www.ivcc.com/ -->
<script>

    (function(h,o,t,j,a,r){

        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};

        h._hjSettings={hjid:1554910,hjsv:6};

        a=o.getElementsByTagName('head')[0];

        r=o.createElement('script');r.async=1;

        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;

        a.appendChild(r);

    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');

</script>

	</head>

	<body <?php body_class(); ?>>
		<a class="skip-to-content-link" href="#main">Skip to content</a>
		<!-- Google Tag Manager (noscript) -->

		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF4DWP6"

		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

		<!-- End Google Tag Manager (noscript) -->


		<?php if(is_front_page()): ?>
			<?php include component('newsletter-popup.php'); ?>
		<?php endif; ?>
		<div class="page-loader"></div>
		<header class="<?php if(is_front_page()): ?>front-page-header<?php endif;?>">
			<div class="top-row">
				<div class="wrap">
					<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow"><?php bloginfo('name'); ?></a>

					<div class="nav-icon">
				 		<span></span>
				 		<span></span>
				 		<span></span>
				 		<span></span>
					</div>

					<?php get_search_form(); ?>
				</div>
			</div>

			<div class="wrap">
				<?php if(is_front_page()): ?>
					<?php include('components/hp-masthead.php'); ?>
				<?php endif; ?>

				<nav>
					<?php get_search_form(); ?>
					<?php wp_nav_menu(array(
						'menu' => __( 'The Main Menu', 'bonestheme' ),
						'container'=> false,
						'theme_location' => 'main-nav',
					)); ?>
				</nav>
			</div>
		</header>

		<?php if(is_front_page()): ?>
			<header class="fixed-header">
				<div class="top-row">
						<div class="wrap">
							<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow"><?php bloginfo('name'); ?></a>

							<div class="nav-icon">
						 		<span></span>
						 		<span></span>
						 		<span></span>
						 		<span></span>
							</div>

							<?php get_search_form(); ?>
						</div>
					</div>

					<div class="wrap">

						<nav>
							<?php get_search_form(); ?>
							<?php wp_nav_menu(array(
								'menu' => __( 'The Main Menu', 'bonestheme' ),
								'container'=> false,
								'theme_location' => 'main-nav',
							)); ?>
						</nav>
					</div>
				</header>
			<?php endif; ?>

			<main id="main">
