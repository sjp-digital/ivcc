<?php
/*
	Template Name: Research & Development Overview
*/
?>

<?php get_header(); ?>

	<div class="section_introduction research-and-dev-intro changeable-intro-content wrap">
		<div class="left-col">
			<h1><?php the_title(); ?></h1>

			<? if( have_rows('grid_category', $post->ID) ): ?>
				<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
					<? $cat_title = get_sub_field('category_title',$post->ID); ?>
					<? $cat_title = str_replace(' ', '-', $cat_title); ?>
					<? $cat_title = strtolower($cat_title); ?>
					<div class="content" grid-related="<?= $cat_title; ?>">
						<?php the_sub_field('related_content', $post->ID); ?>
					</div>
				<?php endwhile; ?>
			<? endif; ?>
		</div>

		<div class="map active-dots active-map">
			<?php include svg('projects.php'); ?>

			<? while ( have_rows('add_who_we_work_with_location', 6) ) : the_row(); ?>
				<? $location = get_sub_field('location', 6); ?>
				<div class="location <? the_sub_field('size_of_marker', 6) ?> <?= $location['value']; ?>">
					<div class="location-details">
						<? while ( have_rows('organisation_type', 6) ) : the_row(); ?>
							<div class="body-of-work">
								<? if(get_sub_field('specific_location', 6)): ?>
									<h4><? the_sub_field('specific_location', 6); ?></h4>
								<? endif; ?>
								<h5><? the_sub_field('organisation_type', 6); ?></h5>

								<? while ( have_rows('organisation', 6) ) : the_row(); ?>
									<p><a href="<? the_sub_field('organisation_link', 6); ?>"><? the_sub_field('organisation_title', 6); ?></a></p>
								<? endwhile; ?>
							</div>
						<? endwhile; ?>
					</div>
					<div class="location-marker"></div>
				</div>
			<? endwhile; ?>
		</div>
	</div>

	<div class="grid-container">
		<div class="wrap">
			<? if( have_rows('grid_category', $post->ID) ): ?>
				<div class="filters-container mobile-filter updated-filters">
					<div class="mobile-filter-trigger">
						<p>Filter By:</p>
						<img src="<?php image('red-chevron.svg') ?>">
					</div>
					<ul>
						<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
							<? $cat_title = get_sub_field('category_title',$post->ID); ?>
							<? $cat_title = str_replace(' ', '-', $cat_title); ?>
							<? $cat_title = strtolower($cat_title); ?>

							<li class="filter <?= $cat_title; ?>-filter" grid-related="<?= $cat_title; ?>"><? the_sub_field('category_title', $post->ID); ?></li>
						<?php endwhile; ?>
					</ul>
				</div>
			<? endif; ?>

			<? if( have_rows('grid_category', $post->ID) ): ?>
				<div class="filters-container desktop-filters updated-filters">
					<p>FILTER:</p>
					<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
						<? $cat_title = get_sub_field('category_title',$post->ID); ?>
						<? $cat_title = str_replace(' ', '-', $cat_title); ?>
						<? $cat_title = strtolower($cat_title); ?>
						<div class="single-filter-group-container">
							<div class="filter <?= $cat_title; ?>-filter <?php the_sub_field('filter_colour', $post->ID) ?>" grid-related="<?= $cat_title; ?>"><? the_sub_field('category_title', $post->ID); ?></div>
						</div>
					<?php endwhile; ?>
				</div>
			<? endif; ?>
		</div>

		<? if( have_rows('grid_category', $post->ID) ): ?>
			<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
				<? $cat_title = get_sub_field('category_title',$post->ID); ?>
				<? $cat_title = str_replace(' ', '-', $cat_title); ?>
				<? $cat_title = strtolower($cat_title); ?>
				<div class="grid-category-container" grid-related="<?= $cat_title; ?>">
					<? if(get_sub_field('category_image', $post->ID)): ?>
						<div class="wrap">
							<img class="category-image" alt="<? the_sub_field('category_title',$post->ID); ?> Image" src="<? the_sub_field('category_image', $post->ID); ?>">
						</div>
					<? endif; ?>
					<div class="grid wrap">
						<div class="grid-sizer"></div>
						<div class="gutter-sizer"></div>
						<? the_sub_field('category_title', $post->ID); ?>
						<?php $posts = get_sub_field('inner_grid_selection', $post->ID); ?>
						<?php $posttype = 'any'; ?>
						<?php get_overview_grid($posttype, $posts) ?>
					</div>
				</div>
			<? endwhile; ?>
		<? endif; ?>

	</div>
</div>

<?php get_footer(); ?>
