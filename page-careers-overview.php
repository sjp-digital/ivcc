<?php
/*
	Template Name: Careers Overview
*/
?>

<?php get_header(); ?>

	<div class="section_introduction vector-control-introduction wrap">
		<div class="left-col">
			<h1><?php the_title(); ?></h1>

			<div class="content careers-overview-content">
				<?php the_field('introductory_content', $post->ID); ?>
			</div>
			<div class="author-info">
				<?php $avatar = get_field('contact_picture', $post->ID ); ?>
				<img class="author-pic" src="<?php echo $avatar; ?>">
				<div class="author-details">
					<?php the_field('contact_name', $post->ID); ?><br>
					<a href="mailto:<?php the_field('contact_email_address', $post->ID); ?>"><?php the_field('contact_email_address', $post->ID); ?></a>
				</div>
			</div>
		</div>
	</div>

	<div class="grid-container">
		<div class="wrap">

		</div>

		<div class="grid wrap">
			<div class="grid-sizer"></div>
			<div class="gutter-sizer"></div>
			<?php $posttype = 'careers'; ?>
			<? $posts = 0; ?>
			<?php get_overview_grid($posttype, $posts) ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
