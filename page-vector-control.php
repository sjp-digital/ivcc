<?php
/*
	Template Name: Vector Control
*/
?>

<?php get_header(); ?>

	<div class="section_introduction vector-control-introduction changeable-intro-content wrap">
		<div class="left-col">
			<h1><?php the_title(); ?></h1>

			<? if( have_rows('grid_category', $post->ID) ): ?>
				<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
					<? $cat_title = get_sub_field('category_title',$post->ID); ?>
					<? $cat_title = str_replace(' ', '-', $cat_title); ?>
					<? $cat_title = strtolower($cat_title); ?>
					<div class="content" grid-related="<?= $cat_title; ?>">
						<?php the_sub_field('related_content', $post->ID); ?>
					</div>
				<?php endwhile; ?>
			<? endif; ?>
		</div>

		<div class="map active-dots active-map">
			<?php include svg('projects.php'); ?>

			<a href="#mapend" class="maps__skip">Skip Map</a>
			<? while ( have_rows('add_location', 6) ) : the_row(); ?>
				<? $location = get_sub_field('location', 6); ?>
				<div class="location <? the_sub_field('size_of_marker', 6) ?> <?= $location['value']; ?>" tabindex="0" aria-label="Location <?= $location['label']; ?>">
					<div class="location-details">
						<h4><?= $location['label']; ?></h4>

						<? while ( have_rows('body_of_work', 6) ) : the_row(); ?>
							<div class="body-of-work">
								<h5><? the_sub_field('body_of_work_title', 6); ?></h5>

								<? while ( have_rows('site', 6) ) : the_row(); ?>
									<p><a href="<? the_sub_field('site_link', 6); ?>"><? the_sub_field('site_title', 6); ?></a></p>

								<? endwhile; ?>
							</div>
						<? endwhile; ?>
					</div>
					<div class="location-marker"></div>
				</div>
			<? endwhile; ?>
			<div id="mapend" tabindex="0"></div>
		</div>
	</div>

	<div class="grid-container">
		<div class="wrap">

			<? if( have_rows('grid_category', $post->ID) ): ?>
				<div class="filters-container mobile-filter updated-filters">
					<div class="mobile-filter-trigger">
						<p>Filter By:</p>
						<img src="<?php image('red-chevron.svg') ?>">
					</div>
					<ul>
						<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
							<? $cat_title = get_sub_field('category_title',$post->ID); ?>
							<? $cat_title = str_replace(' ', '-', $cat_title); ?>
							<? $cat_title = strtolower($cat_title); ?>

							<li class="filter <?= $cat_title; ?>-filter" grid-related="<?= $cat_title; ?>" tabindex="0" aria-label="Filter by <? the_sub_field('category_title', $post->ID); ?>"><? the_sub_field('category_title', $post->ID); ?></li>
						<?php endwhile; ?>
					</ul>
				</div>
			<? endif; ?>

			<? if( have_rows('grid_category', $post->ID) ): ?>
				<div class="filters-container desktop-filters updated-filters">
					<p>FILTER:</p>
					<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
						<? $cat_title = get_sub_field('category_title',$post->ID); ?>
						<? $cat_title = str_replace(' ', '-', $cat_title); ?>
						<? $cat_title = strtolower($cat_title); ?>
						<div class="single-filter-group-container">
							<div class="filter <?= $cat_title; ?>-filter <?php the_sub_field('filter_colour', $post->ID) ?>" grid-related="<?= $cat_title; ?>" tabindex="0" aria-label="Filter by <? the_sub_field('category_title', $post->ID); ?>"><? the_sub_field('category_title', $post->ID); ?></div>
						</div>
					<?php endwhile; ?>
				</div>
			<? endif; ?>
		</div>

		<? if( have_rows('grid_category', $post->ID) ): ?>
			<? while ( have_rows('grid_category', $post->ID) ) : the_row(); ?>
				<? $cat_title = get_sub_field('category_title',$post->ID); ?>
				<? $cat_title = str_replace(' ', '-', $cat_title); ?>
				<? $cat_title = strtolower($cat_title); ?>
				<div class="grid-category-container" grid-related="<?= $cat_title; ?>">
					<? if(get_sub_field('category_image', $post->ID)): ?>
						<div class="wrap">
							<img class="category-image" alt="<? the_sub_field('category_title',$post->ID); ?> Image" src="<? the_sub_field('category_image', $post->ID); ?>">
						</div>
					<? endif; ?>
					<div class="grid wrap">
						<div class="grid-sizer"></div>
						<div class="gutter-sizer"></div>
						<? the_sub_field('category_title', $post->ID); ?>
						<?php $posts = get_sub_field('inner_grid_selection', $post->ID); ?>
						<?php $posttype = 'any'; ?>
						<?php get_overview_grid($posttype, $posts) ?>
					</div>
				</div>
			<? endwhile; ?>
		<? endif; ?>

	</div>

<?php get_footer(); ?>
