<?php
/*
	Template Name: Flexible Content
*/
?>

<?php get_header(); ?>

	<?php if(get_field('page_navigation') == '1'): ?>
		<div class="wrap page-nav-active">
			<div class="page-nav">
				<div class="page-nav-trigger">
					<p>Read about our...</p>
					<img src="<?php image('red-chevron.svg') ?>">
				</div>
				<ul>
					<?php while ( have_rows('blocks', $post->ID) ) : the_row();
						if( get_row_layout() == 'full_width_image_block'): ?>

						<? elseif(get_row_layout() == 'page_introduction'):
							$blocktitle = get_sub_field('block_title', $post->ID);
							$blocktitle = strtolower($blocktitle);
							$blocktitle = str_replace(" ", "-", $blocktitle);
							$blocktitle = preg_replace('/[^A-Za-z0-9\-]/', '', $blocktitle); ?>

							<? if($blocktitle == 'about-ivcc'): ?>
								<li><a href="#<?php echo $blocktitle; ?>">About IVCC</a></li>
							<? else: ?>
								<li><a href="#<?php echo $blocktitle; ?>">Introduction</a></li>
							<? endif; ?>

						<? elseif(get_sub_field('block_title', $post->ID)):
							$blocktitle = get_sub_field('block_title', $post->ID);
							$blocktitle = strtolower($blocktitle);
							$blocktitle = str_replace(" ", "-", $blocktitle);
							$blocktitle = preg_replace('/[^A-Za-z0-9\-]/', '', $blocktitle); ?>

							<li><a href="#<?php echo $blocktitle; ?>"><?php the_sub_field('block_title', $post->ID); ?></a></li>
						<? endif; ?>
					<?php endwhile; ?>
				</ul>
				<?php if(get_field('show_author_info') == '1'): ?>
					<?php $author_id = get_field('select_author', $post->ID); ?>
					<?php $thumb_id = get_post_thumbnail_id($author_id);
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
					$thumb_url = $thumb_url_array[0]; ?>

					<div class="author-info">
						<img class="author-pic" src="<?php echo $thumb_url; ?>">
						<div class="author-details">
							<?php echo get_the_title($author_id); ?>
							<a href="mailto:<?php the_field('email_address', $author_id) ?>"><?php the_field('email_address', $author_id); ?></a>
						</div>
					</div>
				<?php endif; ?>
			</div>
	<?php else: ?>
		<div class="wrap">
	<?php endif; ?>
		<div class="flexible-content read-time-count">
		<?php while ( have_rows('blocks', $post->ID) ) : the_row();
			$blocktitle = get_sub_field('block_title', $post->ID);
			$blocktitle = strtolower($blocktitle);
			$blocktitle = str_replace(" ", "-", $blocktitle);
			$blocktitle = preg_replace('/[^A-Za-z0-9\-]/', '', $blocktitle);

			if( get_row_layout() == 'page_introduction' ):

					include block('page_introduction.php');

				elseif( get_row_layout() == 'strategy' ):

					include block('strategy.php');

				elseif( get_row_layout() == 'successes' ):

					include block('successes.php');

				elseif( get_row_layout() == 'boards_and_committees' ):

					include block('boards_and_committees.php');

				elseif( get_row_layout() == 'general_text' ):

					include block('general_text.php');

				elseif( get_row_layout() == 'call_out_-_general' ):

					include block('call_out_-_general.php');

				elseif( get_row_layout() == 'call_out_-_features_quote' ):

					include block('call_out_-_features_quote.php');

				elseif( get_row_layout() == 'feature_-_overview' ):

					include block('feature_-_overview.php');

				elseif( get_row_layout() == 'research_papers' ):

					include block('research_papers.php');

				elseif( get_row_layout() == 'team_members' ):

					include block('team_members.php');

				elseif( get_row_layout() == 'download_block' ):

					include block('download_block.php');

				elseif( get_row_layout() == 'funding_block' ):

					include block('funding_block.php');

				elseif( get_row_layout() == 'video_block' ):

					include block('video_block.php');

				elseif( get_row_layout() == 'image_block' ):

					include block('image_block.php');

				elseif( get_row_layout() == 'full_width_image_block' ):

					include block('full_width_image_block.php');

				endif;

			endwhile; ?>
		</div>
	</div>

	<?php if(get_field('show_grid', $post->ID)): ?>
		<? $postID = $post->ID; ?>

		<div class="grid-container related-grid">
			<div class="wrap">
				<h2>Related</h2>
				<div class="grid">
					<div class="grid-sizer"></div>
					<div class="gutter-sizer"></div>

					<? if(get_field('related_posts', $post->ID)): ?>
						<?php $posts = get_field('related_posts', $post->ID); ?>
						<?php $tags = 0; ?>
					<? else: ?>
						<?php $tags = get_field('related_tags', $post->ID); ?>
						<? $posts = 0; ?>
					<? endif; ?>

					<?php get_related_grid($posts, $tags, $postID); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>


<?php get_footer(); ?>
