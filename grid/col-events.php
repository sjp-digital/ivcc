	<?php
		if(get_field('grid_thumbnail', $col_content_id)):
			$image = get_field('grid_thumbnail', $col_content_id);
		endif;
	?>

	<?php $startdate = get_field("start_date", $col_content_id) ?>

	<?php if( strtotime($startdate) > strtotime('now') ):
		$status = 'upcoming';
	else:
		$status = 'archive';
	endif; ?>
<? $newpost = get_field('new_post', $col_content_id); ?>
	<div class="grid-col<? if($newpost):?> new-post<? endif; ?> <?php echo $status; ?> <? if(get_the_tags($col_content_id)): ?> <?php foreach( $posttags as $tag ): ?><?php echo $tag->slug; ?> <?php endforeach; ?><? endif; ?><?php echo get_post_type($col_content_id) ?>-col <?= $row_format ?>-grid-col-<?= $col_no; ?>">
		<?php if($posttags): ?>
			<div class="tag-container">
				<?php foreach( $posttags as $tag ): ?>
					<a class="tag <?php echo $tag->slug; ?>" href="<? url() ?>/?s=<?php echo $tag->name; ?>">#<? echo $tag->name; ?></a>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		<a href="<?= get_permalink($col_content_id) ?>">
			<div class="col-image" style="background: url('<?php echo esc_url($image['url']); ?>') center / cover;">
				<? if(get_field('virtual_event', $col_content_id)): ?>
					<div class="virtual-event">Virtual Event</div>
				<? endif; ?>
			</div>
			<div class="col-content">
				<strong><?php echo date("d/m/Y", strtotime(get_field("start_date", $col_content_id))); ?></strong>
				<h2><?php echo get_the_title($col_content_id) ?></h2>
			</div>
		</a>
	</div>

