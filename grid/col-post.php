<? $posttags = get_the_tags($col_content_id); ?>

<?php
if(get_field('grid_thumbnail', $col_content_id)):
	$image = get_field('grid_thumbnail', $col_content_id);
	$size = 'medium';
	$imgurl = $image['sizes'][ $size ];
endif;
$post_categories = get_the_terms($col_content_id, 'category');
?>

<? if(get_field('newsletter_archive', $post->ID)):
	$link = get_field('newsletter_archive_link', $post->ID);
else:
	$link = get_permalink($col_content_id);
endif; ?>

<? $newpost = get_field('new_post', $col_content_id); ?>

<div class="grid-col<? if($newpost):?> new-post<? endif; ?> <?php if(!empty($post_categories)): foreach( $post_categories as $post_categories ): ?><? echo $post_categories->slug; ?> <?php endforeach; endif; ?> post-col <?= $row_format ?>-grid-col-<?= $col_no; ?>">
	<?php if($posttags): ?>
		<div class="tag-container">
			<?php foreach( $posttags as $tag ): ?>
				<a class="tag <?php echo $tag->slug; ?>" href="<? url() ?>/?s=<?php echo $tag->name; ?>">#<? echo $tag->name; ?></a>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<? if(get_field('newsletter_archive', $post->ID)): ?>
		<a href="<?= $link; ?>" target="blank" class="newsletter-link">
	<? else: ?>
		<a href="<?= $link; ?>">
	<? endif; ?>

		<div class="col-image" style="background: url('<?php echo $imgurl; ?>') center / cover;"></div>
		<div class="col-content">
			<?php 
				$element = 'h2';
				if(is_front_page()) {
					$element = 'h3';
				}
			?>
			<?php if(get_field('grid_title', $col_content_id)): ?>
				<<?php echo $element; ?>><?php the_field('grid_title', $col_content_id) ?></<?php echo $element; ?>>
			<?php else: ?>
				<<?php echo $element; ?>><?php echo get_the_title($col_content_id) ?></<?php echo $element; ?>>
			<?php endif; ?>

			<? if(get_field('newsletter_archive', $post->ID)): ?>
				<?php if(get_field('post_excerpt', $col_content_id)): ?>
					<p><?php the_field('post_excerpt', $col_content_id) ?></p>
					<p class="readmore">View Newsletter ></p>
				<?php endif; ?>
			<? else: ?>
				<?php if(get_field('post_excerpt', $col_content_id)): ?>
					<p><?php the_field('post_excerpt', $col_content_id) ?></p>
					<p class="readmore">Read More ></p>
				<?php endif; ?>
			<? endif; ?>
		</div>
	</a>
</div>
