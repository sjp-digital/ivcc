	<? $title = get_the_title($col_content_id);
	$title = strtolower($title);
	$title = str_replace(' ', '-', $title); ?>

	<? $slug = get_post_field( 'post_name', $col_content_id ); ?>
	<? $posttags = get_the_tags($col_content_id); ?>
	<? $newpost = get_field('new_post', $col_content_id); ?>
	<div class="grid-col bespoke-col<? if($newpost):?> new-post<? endif; ?> <?php echo $slug; ?> <?php echo $title; ?>-bespoke <?= $row_format ?>-grid-col-<?= $col_no; ?> <?php the_field('background_gradient', $col_content_id) ?>-gradient">
		<?php if($posttags): ?>
			<div class="tag-container">
				<?php foreach( $posttags as $tag ): ?>
					<a class="tag <?php echo $tag->slug; ?>" href="<? url() ?>/?s=<?php echo $tag->name; ?>">#<? echo $tag->name; ?></a>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		<a href="<?= get_permalink($col_content_id) ?>">
			<div class="content">
				<img src="<?php the_field('col_logo', $col_content_id) ?>">
				<?php if(get_field('snippet', $col_content_id)): ?>
					<p><?php the_field('snippet', $col_content_id) ?></p>
				<?php endif; ?>

				<p class="readmore">Learn More ></p>
			</div>
		</a>
	</div>
