
	<? $posttags = get_the_tags($col_content_id); ?>
	<? $newpost = get_field('new_post', $col_content_id); ?>

	<div class="grid-col<? if($newpost):?> new-post<? endif; ?> job-vacancy-col <? the_field('grid_colour', $col_content_id) ?> <?php foreach( $posttags as $tag ): ?><?php echo $tag->slug; ?> <?php endforeach; ?><?= $row_format ?>-grid-col-<?= $col_no; ?>">
		<a href="<?= get_permalink($col_content_id) ?>">
			<div class="tag-container">
				<?php if($posttags):
					foreach( $posttags as $tag ): ?>
						<a class="tag <?php echo $tag->slug; ?>" href="<? url() ?>/?s=<?php echo $tag->name; ?>">#<? echo $tag->name; ?></a>
	    			<?php endforeach;
				endif; ?>
			</div>
			<h3>Vacancy</h3>
			<h3><?= get_the_title($col_content_id) ?></h3>
			<h5><? the_field('job_location', $col_content_id) ?></h5>
		</a>
	</div>
