<div class="grid-container">
	<div class="grid wrap">
		<div class="grid-sizer"></div>
		<div class="gutter-sizer"></div>
		<?php while(has_sub_field('hp_grid')):
			$row_format = get_sub_field('row_format_a__b__c');
			$col_no = 1;

			if(!in_array($row_format, ['promo-a','promo-b', 'promo-c'], true )):

			while(has_sub_field('content_columns')):

				if(get_sub_field('select_content', $post->ID)):
					$col_content_id = get_sub_field('column_content', $post->ID);
				else:
					$post_type = get_sub_field('latest_post_type', $post->ID);
					latest_post($post_type);
				endif;

			if(get_post_type($col_content_id) == 'page'):

				include gridelement('col-page.php');

			elseif(get_post_type($col_content_id) == 'events'):

				include gridelement('col-events.php');

		elseif(get_post_type($col_content_id) == 'careers'):

			include gridelement('col-vacancy.php');

			elseif(get_post_type($col_content_id) == 'resources'):

				include gridelement('col-resources.php');

			elseif(get_post_type($col_content_id) == 'post'):

				include gridelement('col-post.php');

			elseif((get_post_type($col_content_id) == 'research-development') || (get_post_type($col_content_id) == 'vector-control') || (get_post_type($col_content_id) == 'market-access')):

				include gridelement('col-post.php');

			elseif((get_the_title($col_content_id) == 'NgenIRS') || get_the_title($col_content_id) =='New Nets Project (NNP)'):

				include gridelement('col-bespoke.php');

			else:

				include gridelement('col-page.php');

			endif;

					$col_no++;
					endwhile;
				else:

				while(has_sub_field('promo_content_columns')):
					$col_content_id = get_sub_field('column_content', $post->ID);

				$posttags = get_the_tags($col_content_id); ?>

					<?php if((get_the_title($col_content_id) == 'Job Vacancies')):

						include gridelement('col-vacancy.php');

						elseif(get_post_type($col_content_id) == 'page'):

						include gridelement('col-page.php');

					elseif(get_post_type($col_content_id) == 'post'):

						include gridelement('col-post.php');

					elseif(get_post_type($col_content_id) == 'events'):

						include gridelement('col-events.php');

					elseif(get_post_type($col_content_id) == 'careers'):

						include gridelement('col-vacancy.php');

					elseif(get_post_type($col_content_id) == 'resources'):

					include gridelement('col-resources.php');

					elseif((get_the_title($col_content_id) == 'NgenIRS') || get_the_title($col_content_id) =='New Nets Project (NNP)'):

						include gridelement('col-bespoke.php');

					else:

						include gridelement('col-post.php');

					endif;

					$col_no++;
				endwhile;
			endif;
		endwhile; ?>
	</div>
</div>
