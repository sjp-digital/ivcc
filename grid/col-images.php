<?php
	if(get_field('grid_thumbnail', $col_content_id)):
		$image = get_field('grid_thumbnail', $col_content_id);
		$size = 'medium';
		$imgurl = $image['sizes'][ $size ];
	endif;
?>
<? $posttags = get_the_tags($col_content_id); ?>
<? $newpost = get_field('new_post', $col_content_id); ?>

<div class="grid-col<? if($newpost):?> new-post<? endif; ?> resources-grid-col <?php foreach( $posttags as $tag ): ?><?php echo $tag->slug; ?> <?php endforeach; ?><?php if($col_categories): foreach( $col_categories as $col_category ): ?> <? echo $col_category->slug; ?> <?php endforeach; endif; ?> images-col <?= $row_format ?>-grid-col-<?= $col_no; ?><? if(get_field('full_size_image', $col_content_id)): ?> full-size-image<? endif; ?>" postid="<?php echo $col_content_id; ?>">
	<?php if($posttags): ?>
		<div class="tag-container">
			<?php foreach( $posttags as $tag ): ?>
				<a class="tag <?php echo $tag->slug; ?>" href="<? url() ?>/?s=<?php echo $tag->name; ?>">#<? echo $tag->name; ?></a>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<a href="#" data-remodal-target="ajax-modal">
		<div class="col-image" style="background: url('<?php echo $imgurl; ?>') center / cover;"></div>
		<div class="col-content">
			<h3><?php echo get_the_title($col_content_id) ?>  <img class="play-icon" src="<? image('icon-image.svg') ?>" alt="Image Icon"></h3>
		</div>
	</a>
</div>
