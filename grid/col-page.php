<?php
	if(get_field('grid_thumbnail', $col_content_id)):
		$image = get_field('grid_thumbnail', $col_content_id);
		$size = 'medium';
		$imgurl = $image['sizes'][ $size ];
	endif;
?>

<? $posttags = get_the_tags($col_content_id); ?>
<? $newpost = get_field('new_post', $col_content_id); ?>

<div class="grid-col<? if($newpost):?> new-post<? endif; ?> page-col <? if((get_field('thumbnail_or_colour', $col_content_id) == 'colour')): ?>no-thumbnail <? endif;?><?= $row_format ?>-grid-col-<?= $col_no; ?> <?php if($posttags): ?><?php foreach( $posttags as $tag ): ?><?php echo $tag->slug; ?> <?php endforeach; ?><? endif; ?>"<? if((get_field('thumbnail_or_colour', $col_content_id) == 'colour')): ?> style="background: <? the_field('grid_colour', $col_content_id); ?>"<? endif;?>>
	<?php if($posttags): ?>
		<div class="tag-container">
			<?php foreach( $posttags as $tag ): ?>
				<a class="tag <?php echo $tag->slug; ?>" href="<? url() ?>/?s=<?php echo $tag->name; ?>">#<? echo $tag->name; ?></a>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<a href="<?= get_permalink($col_content_id) ?>">
		<? if((get_field('thumbnail_or_colour', $col_content_id) == 'thumbnail')): ?>
			<div class="col-image" style="background: url('<?php echo $imgurl; ?>') center / cover;"></div>
		<? endif ?>
		<div class="col-content">
			<?php if(get_field('grid_title', $col_content_id)): ?>
				<h3><?php the_field('grid_title', $col_content_id) ?></h3>
			<?php endif; ?>
		</div>
	</a>
</div>
