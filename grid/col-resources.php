<?php $col_categories = get_the_terms($col_content_id, 'resources-categories'); ?>
<? $posttags = get_the_tags($col_content_id); ?>

<?php if($col_categories):
	$post_format = $col_categories[0]->slug;
endif; ?>

<?php if( in_array($post_format, array('annual-reports','newsletter', 'policies', 'publications', 'presentations', 'ngenirs-evidence', 'zero-by-40', 'ambassador-pack'), true ) ): ?>

	<?php include gridelement('col-pdf.php'); ?>

<?php elseif ($post_format == 'videos'): ?>

	<?php include gridelement('col-videos.php'); ?>

<?php elseif ($post_format == 'images'): ?>

	<?php include gridelement('col-images.php'); ?>

<?php endif; ?>
