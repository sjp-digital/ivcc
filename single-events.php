<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<?php $thumb_id = get_post_thumbnail_id($post->ID);
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', true);
	$thumb_url = $thumb_url_array[0]; ?>
	<?php $posttags = get_the_tags($post->ID); ?>

	<div class="read-time-count single-event-container wrap">
		<div class="post-intro">
			<h1><?php the_title() ?></h1>
			<div class="page-info">
				<?php if(get_field('virtual_event', $post->ID)): ?>
					<div class="virtual-event">Virtual Event</div>
				<?php endif; ?>

				<div class="read-time">READ TIME <strong><span class="eta"></span></strong> <img src="<?php image('read-time.svg') ?>"></div>
				<?php if($posttags): ?>
					<div class="tag-container">
						<?php foreach( $posttags as $tag ): ?>
		    				<a class="tag <? echo $tag->slug; ?>" href="<? url() ?>/?s=<?php echo $tag->slug; ?>">#<? echo $tag->name; ?></a>
		    			<?php endforeach; ?>
	    			</div>
				<?php endif; ?>
				<?php if(get_field('event_website_link', $post->ID)): ?>
					<a href="<?php the_field('event_website_link', $post->ID) ?>" class="register-for-event" target="_blank"><img src="<?php image('link-icon.svg') ?>"></a>
				<?php endif; ?>
			</div>
		</div>

		<?php if(get_field('event_logo', $post->ID)): ?>
			<img class="event-logo desktop-event-logo" src="<?php the_field('event_logo', $post->ID) ?>">
		<?php endif; ?>

		<div class="post-inner">
			<div class="content">
				<div class="event-details">
					<?php if(get_field('event_logo', $post->ID)): ?>
						<img class="event-logo mobile-event-logo" src="<?php the_field('event_logo', $post->ID) ?>">
					<?php endif; ?>

					<?php $originalDate = get_field('start_date');
					$startDate = date("d F Y", strtotime($originalDate));	?>

					<?php $endDate = get_field('end_date');
					$endDate = date("d F Y", strtotime($endDate));	?>
					<? if(get_field('virtual_event', $post->ID)): ?>
						<? $location = '<strong>Virtual Event</strong>' ?>
					<? else: ?>
						<?php $location = get_field('event_location', $post->ID) ?>
						<? $location = $location['label']; ?>
					<? endif; ?>
					<h4><?php echo $startDate; ?> - <?php echo $endDate; ?><br><?= $location; ?></h4>
				</div>

				<?php the_content(); ?>

				<? if(get_field('downloads_active')): ?>
					<div class="research_papers block">
						<? if(get_field('downloads_title', $post->ID)): ?>
							<h3><?php the_field('downloads_title', $post->ID); ?></h3>
						<? endif; ?>

						<div class="papers-carousel owl-carousel">
								<?php $associated_papers = get_field('associated_papers', $post->ID); ?>
								<? if(get_field('associated_papers', $post->ID)): ?>
									<?php get_research_papers($associated_papers); ?>
								<? endif; ?>
							</div>
						</div>
					<? endif; ?>
				</div>

				<?php social_share(array('f', 't', 'l'), $post); ?>
			</div>

			<div class="sidebar">
				<?php if(get_field('event_attendees',$post->ID)): ?>
				<h4>In attendance</h4>
					<?php $attendees = get_field('event_attendees',$post->ID); ?>

					<?php foreach($attendees as $attendee): ?>
						<?php $thumb_id = get_post_thumbnail_id($attendee);
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
						$thumb_url = $thumb_url_array[0]; ?>

						<div class="author-info">
							<img class="author-pic" src="<?php echo $thumb_url; ?>">
							<div class="author-details">
								<?php echo get_the_title($attendee); ?><br>
								<?php if(get_field('company', $attendee)):
									echo the_field('company', $attendee); ?><br>
								<? endif; ?>
								<a href="mailto:<?php the_field('email_address', $attendee) ?>"><?php the_field('email_address', $attendee); ?></a>
							</div>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php if(get_field('event_website_link', $post->ID)): ?>
					<a href="<?php the_field('event_website_link', $post->ID) ?>" class="register-for-event" target="_blank"><img src="<?php image('link-icon.svg') ?>">Register for this event / visit the website</a>
				<?php endif; ?>
			</div>
		</div>
	</div>

<?php endwhile; ?>


<?php get_footer(); ?>
