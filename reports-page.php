<?php
/*
	Template Name: Reports Page
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<?php $posttags = get_the_tags($post->ID); ?>

	<div class="read-time-count single-post-container wrap">
		<div class="post-intro">
			<h1><?php the_title() ?></h1>
			<div class="page-info">
				<div class="date"><?php echo get_the_date('jS F Y') ?></div>
				<div class="read-time">READ TIME <strong><span class="eta"></span></strong> <img src="<?php image('read-time.svg') ?>"></div>
				<?php if($posttags): ?>
					<div class="tag-container">
						<?php foreach( $posttags as $tag ): ?>
		    				<div class="tag <? echo $tag->slug; ?>">#<? echo $tag->name; ?></div>
		    			<?php endforeach; ?>
	    			</div>
				<?php endif; ?>
			</div>
		</div>

		<div class="post-inner">
			<div class="content">
				<?php the_content(); ?>
			</div>

			<div class="sidebar">
				<?php if(get_field('downloads',$post->ID)): ?>
					<h4>Resources</h4>
					<?php $downloads = get_field('downloads',$post->ID); ?>

					<?php foreach($downloads as $download):
						$attachment_id = get_field('file', $download);
						$url = wp_get_attachment_url( $attachment_id );
						$filesize = filesize( get_attached_file( $attachment_id ) );
						$filesize = size_format($filesize);
						$path_info = pathinfo( get_attached_file( $attachment_id ) ); ?>

						<div class="article-download">
							<img class="pdf-icon" src="<?php image('article-download.svg') ?>">
							<div class="paper-content">
								<p><strong><?php echo get_the_title($download); ?></strong>
								Size <?php echo $filesize; ?>, <span><?php echo $path_info['extension']; ?></span></p>

								<div class="paper-actions">
									<div class="download"><a href="<?php echo $url; ?>">DOWNLOAD <img alt="Download" src="<?php image('icon-download.svg') ?>"></a></div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>

<?php endwhile; ?>


<?php get_footer(); ?>
