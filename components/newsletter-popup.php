<div class="newsletter-popup-container">
	<div class="newsletter-popup">
		<div class="close">X</div>
		<div class="wrap">
			<h3>Sign up to receive the IVCC Newsletter</h3>
			<?php echo do_shortcode('[contact-form-7 id="845" title="Newsletter Signup"]'); ?>

			<div class="pop-up-content">
				<p><? the_field('newsletter_content', 'option'); ?></p>
			</div>
		</div>
	</div>
</div>

