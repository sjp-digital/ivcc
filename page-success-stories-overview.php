<?php
/*
	Template Name: Success Stories Overview
*/
?>

<?php get_header(); ?>

	<div class="success-stories-overview-container wrap">
		<h1><?php the_title(); ?></h1>
		<? the_field('success_stories_introduction',$post->ID); ?>
	</div>

	<div class="grid-container">
		<div class="grid wrap">
			<div class="grid-sizer"></div>
			<div class="gutter-sizer"></div>
			<?php $posttype = 'successes'; ?>
			<? if(get_field('grid_selection', $post->ID)): ?>
				<?php $posts = get_field('grid_selection', $post->ID); ?>
			<? else: ?>
				<? $posts = 0; ?>
			<? endif; ?>
			<?php get_overview_grid($posttype, $posts) ?>
		</div>
	</div>

<?php get_footer(); ?>
