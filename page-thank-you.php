<?php
/*
	Template Name: Thank You Page
*/
?>

<?php get_header(); ?>

<div class="thank-you-container wrap">
	<h1><? the_field('title', $post->ID); ?></h1>
	<p><? the_field('content', $post->ID); ?></p>
	<?php get_search_form(); ?>
</div>


<?php get_footer(); ?>
