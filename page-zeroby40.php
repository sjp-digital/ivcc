<?php
/*
	Template Name: Zero By 40
*/
?>

<?php get_header(); ?>

	<div class="zero-by-40-intro section_introduction wrap">
		<div class="left-col">
			<div class="zero-by-40-intro-content" data-related="general">
				<h1><?php the_title(); ?></h1>

				<div class="content">
					<?php the_field('introductory_content', $post->ID); ?>
				</div>
			</div>
		</div>
		<a href="https://zeroby40.com/" target="_blank" class="zbf-logo">
			<img src="<?php the_field('zero_by_40_logo', $post->ID); ?>" alt="Zero by 40 Logo">
		</a>
	</div>

	<div class="grid-container">
		<div class="wrap zero-by-40-header fadeIn" >
			<a href="https://zeroby40.com/" target="_blank">
				<img src="<? the_field('background_image', $post->ID) ?>" class="ZERO by 40 helps develop innovative vector control tools to eradicate malaria. We believe that together, we can change the work. Visit the ZERO by 40 website.">
			</a>
		</div>
		<div class="grid wrap">
			<div class="grid-sizer"></div>
			<div class="gutter-sizer"></div>

			<?php $posttype = 'any'; ?>
			<? if(get_field('grid_selection', $post->ID)): ?>
				<?php $posts = get_field('grid_selection', $post->ID); ?>
			<? else: ?>
				<? $posts = 0; ?>
			<? endif; ?>
			<?php get_overview_grid($posttype, $posts) ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
