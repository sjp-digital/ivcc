<?php
/*
	Template Name: Resources Overview
*/
?>

<?php get_header(); ?>

	<div class="section_introduction vector-control-introduction wrap">
		<div class="left-col">
			<h1><?php the_title(); ?></h1>

			<div class="content">
				<?php the_field('introductory_content', $post->ID); ?>
			</div>
		</div>

	</div>

	<div class="grid-container">
		<div class="wrap">
			<div class="filters-container desktop-filters">
				<p>FILTER:</p>
				<div class="single-filter-group-container">
					<div class="filter all-filter active-filter" tabindex="0" aria-label="Show all">All</div>
				</div>

				<?php $categories = get_terms('resources-categories');
				$categoryHierarchy = array();
				sort_terms_hierarchicaly($categories, $categoryHierarchy); ?>

				<?php foreach($categoryHierarchy as $category): ?>
					<div class="single-filter-group-container">
						<div class="filter" data-related=".<?php echo $category->slug; ?>" tabindex="0" aria-label="Filter by <?php echo $category->name; ?>"><?php echo $category->name; ?></div>

						<?php if($category->children): ?>
							<div class="sub-filters">
								<?php foreach($category->children as $category): ?>
									<div class="filter sub-filter" data-related=".<?php echo $category->slug; ?>" tabindex="0" aria-label="Filter by <?php echo $category->name; ?>"><?php echo $category->name; ?></div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>

			<div class="filters-container mobile-filter">
				<div class="mobile-filter-trigger">
					<p>Filter By:</p>
					<img src="<?php image('red-chevron.svg') ?>">
				</div>

				<?php $categories = get_terms('resources-categories');
				$categoryHierarchy = array();
				sort_terms_hierarchicaly($categories, $categoryHierarchy); ?>

				<ul>
					<?php foreach($categoryHierarchy as $category): ?>
						<?php if(!$category->children): ?>
							<li class="filter" data-related=".<?php echo $category->slug; ?>"><?php echo $category->name; ?></li>
						<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>

		<div class="grid wrap resources-grid">
			<div class="grid-sizer"></div>
			<div class="gutter-sizer"></div>
			<? if(get_field('grid_selection', $post->ID)): ?>
				<?php $posts = get_field('grid_selection', $post->ID); ?>
			<? else: ?>
				<? $posts = 0; ?>
			<? endif; ?>
			<?php $posttype = 'resources'; ?>
			<?php get_overview_grid($posttype, $posts) ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
