<?php
/*
	Template Name: Job Vacancies
*/
?>

<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>
	<div class="single-job-container wrap">
		<div class="post-intro">
			<h1><?php the_field('vacancy_title', $post->ID); ?></h1>

			<p>Job Location: <?php the_field('job_location', $post->ID); ?><br>
				<?php if(get_field('job_salary', $post->ID)): ?>
					Salary: <?php the_field('job_salary', $post->ID); ?><br>
				<?php endif; ?>
				<?php if(get_field('application_deadline', $post->ID)): ?>
					Application Deadline: <?php the_field('application_deadline', $post->ID); ?><br>
				<?php endif; ?>
			Send applications to: <?php the_field('send_applications_to', $post->ID); ?></p>

			<?php $author_id = get_field('select_recipient', $post->ID); ?>
			<?php $thumb_id = get_post_thumbnail_id($author_id);
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
			$thumb_url = $thumb_url_array[0]; ?>

			<div class="recipient-info">
				<img class="recipient-pic" src="<?php echo $thumb_url; ?>">
				<div class="recipient-details">
					<?php echo get_the_title($author_id); ?><br>
					<a href="mailto:<?php the_field('email_address', $author_id) ?>"><?php the_field('email_address', $author_id); ?></a>
				</div>
			</div>
		</div>

		<div class="post-inner">
			<?php the_field('job_description', $post->ID); ?>
		</div>
	</div>

<?php endwhile; ?>


<?php get_footer(); ?>
