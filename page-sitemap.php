<?php
/*
	Template Name: Sitemap Page
*/
?>

<?php get_header(); ?>

	<div class="sitemap-page">
		<div class="wrap">
			<h1>Sitemap</h1>

			<div class="sitemap-columns">
				<? while ( have_rows('sitemap_groups', $post->ID) ) : the_row(); ?>
					<div class="sitemap-col">
						<h4><? the_sub_field('sitemap_title'); ?></h4>
						<? $sitemap_links = get_sub_field('sitemap_link', $post->ID); ?>

						<ul>
						<? foreach($sitemap_links as $sitemap_item): ?>
							<li><a href="<?= get_permalink($sitemap_item) ?>"><?= get_the_title($sitemap_item) ?></a>
						<? endforeach ?>
						</ul>
					</div>
				<? endwhile; ?>

				<div class="sitemap-col">
					<h4>Events</h4>
					<?= do_shortcode('[wp_sitemap_page only="events"]'); ?>
				</div>
				<div class="sitemap-col blog-col">
					<h4>Posts</h4>
					<?= do_shortcode('[wp_sitemap_page only="post"]'); ?>
				</div>
			</div>
		</div>
	</div>

<? get_footer();
