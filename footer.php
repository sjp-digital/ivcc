			<? if(is_single('ngenirs')): ?>
				<div class="newsletter-sign-up">
					<div class="wrap">
						<h3>Sign up to receive the IVCC Newsletter</h3>
						<?php echo do_shortcode('[contact-form-7 id="2604" title="NGENIRS - Newsletter Signup"]'); ?>
					</div>
				</div>
			<? else: ?>
				<div class="newsletter-sign-up">
					<div class="wrap">
						<h3>Sign up to receive the IVCC Newsletter</h3>
						<?php echo do_shortcode('[contact-form-7 id="845" title="Newsletter Signup"]'); ?>
					</div>
				</div>
			<? endif; ?>
			</main>
			<footer>
				<div class="wrap">
					<div class="nav col">
						<?php wp_nav_menu(array(
							'menu' => __( 'Footer Links', 'bonestheme' ),
							'theme_location' => 'footer-links',
						)); ?>
						<div class="mobile-links">
							<a href="tel:<?php the_field('telephone_number', 'option'); ?>"><?php the_field('telephone_number', 'option'); ?></a>
							<a href="mailto:<?php the_field('email_address', 'option'); ?>"><?php the_field('email_address', 'option'); ?></a>
						</div>
					</div>

					<div class="col">
						<a href="tel:<?php the_field('telephone_number', 'option'); ?>"><?php the_field('telephone_number', 'option'); ?></a>
						<a href="mailto:<?php the_field('email_address', 'option'); ?>"><?php the_field('email_address', 'option'); ?></a>
					</div>
					<div class="col">
						<img alt="IVCC" src="<?php image('white-logo.svg') ?>">
						<a href="https://zeroby40.com/" target="_blank"><img alt="Eradicating Malaria by 2040" src="<?php image('footer-zero-logo.svg') ?>"></a>

						<div class="social-container">
							<?php if(get_field('facebook','option')): ?><a target="_blank"href="<?php the_field('facebook', 'option'); ?>" aria-label="Facebook"><i class="fa fa-facebook-square" aria-hidden="true"></i></a><?php endif; ?>
							<?php if(get_field('linkedin','option')): ?><a target="_blank"href="<?php the_field('linkedin', 'option'); ?>" aria-label="LinkedIn"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a><?php endif; ?>
							<?php if(get_field('twitter','option')): ?><a target="_blank"href="<?php the_field('twitter', 'option'); ?>" aria-label="Twitter"><i class="fa fa-twitter-square" aria-hidden="true"></i></a><?php endif; ?>
							<?php if(get_field('vimeo','option')): ?><a target="_blank"href="<?php the_field('vimeo', 'option'); ?>" aria-label="Vimeo"><i class="fa fa-vimeo-square" aria-hidden="true"></i></a><?php endif; ?>
							<?php if(get_field('youtube','option')): ?><a target="_blank"href="<?php the_field('youtube', 'option'); ?>" aria-label="YouTube"><i class="fa fa-youtube-square" aria-hidden="true"></i></a><?php endif; ?>
						</div>

						<p class="copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></p>
						<p class="rowan-link">Created by <a href="http://rowanmarketing.co.uk" target="_blank">Rowan Marketing</a></p>
						<p class="company-details">IVCC is a company registered in England and Wales with Company No.06719882. Registered office address is Liverpool School Of Tropical Medicine, Pembroke Place, Liverpool, Merseyside, L3 5QA.</p>
					</div>
					</div>
			</footer>

			<div class="remodal ajax-modal" data-remodal-id="ajax-modal" tabindex="0" aria-label="Close">
				<div class="modal-background">
				<div class="modal-loader">
					<div class="loader">
						<span class="loader-block"></span>
				 		<span class="loader-block"></span>
				 		<span class="loader-block"></span>
				 		<span class="loader-block"></span>
				 		<span class="loader-block"></span>
				 		<span class="loader-block"></span>
				 		<span class="loader-block"></span>
				 		<span class="loader-block"></span>
				 		<span class="loader-block"></span>
				 	</div>
				</div>

					<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
					<div class="modal-inner">
						<div class="modal-content">

						</div>
					</div>
				</div>
			</div>

<script>
window.REMODAL_GLOBALS = {
  DEFAULTS: {
    hashTracking: false
  }
};
</script>

		<?php wp_footer(); ?>

	</body>

</html>
