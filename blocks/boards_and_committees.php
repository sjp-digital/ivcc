<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>
<div class="boards_and_committees block">
	<h3><?php the_sub_field('block_title', $post->ID); ?></h3>

	<div class="left-col">
		<?php the_sub_field('boards_and_committees_content', $post->ID); ?>
	</div>
	<?php if(get_sub_field('boards_and_committees_members', $post->ID)): ?>
		<div class="members">
			<?php $members = get_sub_field('boards_and_committees_members', $post->ID); ?>
			<?php foreach($members as $member): ?>
				<?php $thumb_id = get_post_thumbnail_id($member);
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
				$thumb_url = $thumb_url_array[0]; ?>

				<div class="member">
					<img class="member-pic" src="<?php echo $thumb_url; ?>">
					<div class="content">
						<?php echo get_the_title($member); ?><br>
						<a href="mailto:<?php the_field('email_address', $member) ?>"><?php the_field('email_address', $member); ?></a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>

