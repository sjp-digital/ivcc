<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>

<div class="strategy block">
	<h3><?php the_sub_field('block_title', $post->ID); ?></h3>
	<?php the_sub_field('strategy_introductory_content', $post->ID); ?>

	<div class="lower-section">
		<div class="content-col">
			<?php the_sub_field('strategy_main_content', $post->ID); ?>
		</div>

		<?php if(get_sub_field('strategy_image', $post->ID)): ?>
			<?php if(get_sub_field('graphical_image', $post->ID)): ?>
				<img src="<?php the_sub_field('strategy_image', $post->ID); ?>">
			<?php else: ?>
				<div class="image" style="background: url('<?php the_sub_field('strategy_image', $post->ID); ?>') center / cover;"></div>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>
