<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>

	<div class="research_papers block">
		<h3><?php the_sub_field('block_title', $post->ID); ?></h3>
		<div class="intro-content">
			<?php the_sub_field('research_papers_intro_content', $post->ID); ?>
		</div>

		<div class="papers-carousel owl-carousel">
			<?php $associated_papers = get_sub_field('associated_papers', $post->ID); ?>
			<?php get_research_papers($associated_papers); ?>
		</div>
	</div>

