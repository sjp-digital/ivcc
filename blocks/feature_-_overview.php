<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>
<div class="feature_-_overview block <?php the_sub_field('feature_-_overview_image_position', $post->ID); ?>">
	<h3><?php the_sub_field('block_title', $post->ID); ?></h3>
	<div class="upper-content-container">
		<?php if(get_sub_field('feature_-_overview_image', $post->ID)): ?>
			<?php if(get_sub_field('graphical_image', $post->ID)): ?>
				<img src="<?php the_sub_field('feature_-_overview_image', $post->ID); ?>">
			<?php else: ?>
				<div class="image" style="background: url('<?php the_sub_field('feature_-_overview_image', $post->ID); ?>') center / cover;"></div>
			<?php endif; ?>
		<?php endif; ?>
		<div class="content">
			<?php the_sub_field('feature_-_overview_introductory_content', $post->ID); ?>
		</div>
	</div>
	<div class="lower-content-container">
		<div class="content">
			<?php the_sub_field('feature_-_overview_lower_content', $post->ID); ?>
		</div>
		<div class="image-col">
			<?php if( have_rows('feature_-_overview_lower_images') ):
				while ( have_rows('feature_-_overview_lower_images') ) : the_row(); ?>
					<div class="image" style="background: url('<?php the_sub_field('feature_-_overview_lower_images_lower_image', $post->ID) ?>') center / cover;"></div>
				<?php endwhile;
			endif; ?>
		</div>
	</div>
</div>
