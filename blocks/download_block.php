<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>

<div class="download-block block">
	<h3><?php the_sub_field('block_title', $post->ID); ?></h3>

	<div class="inner-container">
		<div class="content">
			<?php the_sub_field('downloads_block_content'); ?>
		</div>

		<?php if(get_sub_field('downloads',$post->ID)): ?>
			<div class="download-column">
				<?php $downloads = get_sub_field('downloads',$post->ID); ?>

				<?php foreach($downloads as $download):
					$attachment_id = get_field('file', $download);
					$url = wp_get_attachment_url( $attachment_id );
					$filesize = filesize( get_attached_file( $attachment_id ) );
					$filesize = size_format($filesize);
					$path_info = pathinfo( get_attached_file( $attachment_id ) ); ?>

					<div class="single-download">
						<img class="pdf-icon" src="<?php image('article-download.svg') ?>">
						<div class="paper-content">
							<p><strong><?php echo get_the_title($download); ?></strong>
							Size <?php echo $filesize; ?>, <span><?php echo $path_info['extension']; ?></span></p>

							<div class="paper-actions">
								<div class="download"><a href="<?php echo $url; ?>">DOWNLOAD <img alt="Download" src="<?php image('icon-download.svg') ?>"></a></div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>
