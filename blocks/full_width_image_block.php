<a class="anchor-link" id="fullwidthimageblock"></a>
<div class="full-width-image-block block">
	<?php
	$image = get_sub_field('image', $post->ID); ?>
	<a href="<?php echo $image['url']; ?>"data-fancybox="images" class="fancybox-link">
		<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</a>
</div>
