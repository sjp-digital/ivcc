<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>

<div class="funding-block block">
	<h2><?php the_sub_field('block_title', $post->ID); ?></h2>
	<?php the_sub_field('block_intro', $post->ID); ?>

	<div class="partners-container <? the_sub_field('number_of_icons_per_line', $post->ID) ?>">
		<? while ( have_rows('funding_partners') ) : the_row(); ?>
			<? $image = get_sub_field('partner_logo'); ?>
			<a href="<? the_sub_field('partner_link'); ?>" target="_blank">
				<img src="<? echo $image['sizes']['medium']; ?>" alt="<? echo $image['alt']; ?>">
			</a>
		<? endwhile; ?>
	</div>
</div>
