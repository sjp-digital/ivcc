<?php $posttags = get_the_tags($post->ID); ?>

<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>

<div class="block page_introduction block">
	<div class="page_intro_content_container">
		<?php if(get_sub_field('icon_title')): ?>
			<img class="icon-title" src="<?php the_sub_field('icon_title_image', $post->ID); ?>">
		<?php else: ?>
			<h1><?php the_sub_field('block_title', $post->ID); ?></h1>
		<?php endif; ?>
		<div class="page-info">
			<div class="read-time">READ TIME <strong><span class="eta"></span></strong> <img src="<?php image('read-time.svg') ?>"></div>
			<?php if($posttags): ?>
				<div class="tag-container">
					<?php foreach( $posttags as $tag ): ?>
	    				<a class="tag <? echo $tag->slug; ?>" href="<? url() ?>/?s=<?php echo $tag->slug; ?>">#<? echo $tag->name; ?></a>
	    			<?php endforeach; ?>
    			</div>
			<?php endif; ?>
			<?php if(get_sub_field('page_download',$post->ID)):
				$paperid = get_sub_field('page_download_post',$post->ID);
				$attachment_id = get_field('file', $paperid);
				$url = wp_get_attachment_url( $attachment_id ); ?>

				<div class="download">
					<a href="<?php echo $url; ?>">
						<strong>DOWNLOAD</strong> <img src="<?php image('icon-download-grey.svg') ?>">
					</a>
				</div>
			<?php endif; ?>
		</div>

		<div class="content">
			<?php the_sub_field('page_introduction_content', $post->ID); ?>
		</div>
	</div>
</div>


