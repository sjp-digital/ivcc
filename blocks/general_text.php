<?php $numberofcols = get_sub_field('general_text_number_of_columns', $post->ID); ?>

<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>

<div class="general_text block <?php echo $numberofcols; ?>">
	<h2><?php the_sub_field('block_title', $post->ID); ?></h2>

	<?php if($numberofcols =="one"): ?>

		<?php the_sub_field('general_text_content', $post->ID); ?>

	<?php else: ?>

		<div class="intro-content">
			<?php the_sub_field('general_text_introductory_content', $post->ID); ?>
		</div>

		<div class="left-col">
			<?php the_sub_field('general_text_left_column_content', $post->ID); ?>
		</div>
		<div class="right-col">
			<?php the_sub_field('general_text_right_column_content', $post->ID); ?>
		</div>

	<?php endif; ?>

</div>
