<a class="anchor-link" id="imageblock"></a>
<div class="image-block block">

	<?php

	$images = get_sub_field('images');
	$size = 'medium';

	foreach( $images as $image ): ?>
		<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
	<?php endforeach; ?>
</div>
