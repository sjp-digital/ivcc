<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>
<div class="video-block block">
	<h3><? the_sub_field('block_title', $post->ID); ?></h3>
	<? the_sub_field('introductory_content', $post->ID); ?>
	<? $videourl = get_sub_field('video_url', $post->ID); ?>
	<?php $embed_code = wp_oembed_get($videourl); ?>

	<? echo $embed_code; ?>
</div>
