<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>

<div class="call_out_-_features_quote block <?php the_sub_field('call_out_-_features_quote_image_position', $post->ID); ?>">
	<h3><?php the_sub_field('block_title', $post->ID); ?></h3>
	<div class="content-container">
		<?php if(get_sub_field('call_out_-_features_quote_image', $post->ID)): ?>
			<?php if(get_sub_field('graphical_image', $post->ID)): ?>
				<img src="<?php the_sub_field('call_out_-_features_quote_image', $post->ID); ?>">
			<?php else: ?>
				<div class="image" style="background: url('<?php the_sub_field('call_out_-_features_quote_image', $post->ID); ?>') center / cover;"></div>
			<?php endif; ?>
		<?php endif; ?>
		<div class="content">
			<?php the_sub_field('call_out_-_features_quote_content', $post->ID); ?>
		</div>
	</div>
	<div class="quote">
		<?php the_sub_field('call_out_-_features_quote_quote', $post->ID); ?>
	</div>
</div>
