<a class="anchor-link" id="<?php echo $blocktitle; ?>"></a>

<div class="block team-members">
	<h2><?php the_sub_field('block_title'); ?></h2>
	<? the_sub_field('intro_content', $post->ID); ?>
	<?php $post_object = get_sub_field('team_members', $post->ID); ?>

	<div class="team-members-grid">
		<?php foreach( $post_object as $post): ?>
			<div class="team-member">
				<?php 
					$thumb_id = get_post_thumbnail_id($post->ID);
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
					$thumb_url = $thumb_url_array[0];

					if(get_field('shorthand_name', $post->ID)) {
						$name = get_field('shorthand_name', $post->ID);
					} else {
						$name = get_the_title($post->ID);
						$name= preg_replace('/\W\w+\s*(\W*)$/', '$1', $name);
					}
				?>
				<div class="profile-picture" style="background: url('<?php echo $thumb_url; ?>') center / cover;">
					<a href="#" data-remodal-target="ajax-modal" class="team-modal-link" postid="<?= $post->ID ?>" aria-label="<?php echo get_the_title($post->ID) . ', ' . get_field('job_title', $post->ID) . ' biography'; ?>">
						<div class="overlay">
							<p><?php echo $name ?>'s Profile</p>
						</div>
					</a>
				</div>
				<p><strong><?php echo get_the_title($post->ID); ?></strong><br>
				<?php the_field('job_title', $post->ID); ?><br>
				<?php if(get_field('email_address', $post->ID)) : ?>
					<a href="mailto:<?php the_field('email_address', $post->ID); ?>"><?php the_field('email_address', $post->ID); ?></a></p>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
		<?php wp_reset_postdata(); ?>
	</div>
</div>

<?php wp_reset_postdata(); ?>
